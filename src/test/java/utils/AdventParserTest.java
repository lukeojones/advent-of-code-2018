package utils;

import org.junit.Assert;
import org.junit.Test;

import java.util.BitSet;

import static org.junit.Assert.*;

public class AdventParserTest {
    @Test
    public void getTokens() throws Exception {

    }

    @Test
    public void getNumericTokens() throws Exception {
        AdventParser parser =  AdventParser.ofRegex("position=<(.*),(.*)> velocity=<(.*),(.*)>");
        int[] numericTokens = parser.getNumericTokens("position=< 21992, -10766> velocity=<-2,  1>");

        Assert.assertEquals(21992, numericTokens[0]);
        Assert.assertEquals(-10766, numericTokens[1]);
        Assert.assertEquals(-2, numericTokens[2]);
        Assert.assertEquals(1, numericTokens[3]);
    }

    @Test
    public void getCharTokens() throws Exception {
        AdventParser parser =  AdventParser.ofRegex("Step (.*) must be finished before step (.*) can begin.");
        Character[] charTokens = parser.getCharTokens("Step C must be finished before step A can begin.");

        Assert.assertEquals('C', (char)charTokens[0]);
        Assert.assertEquals('A', (char)charTokens[1]);
    }

    @Test
    public void getPotTokens() throws Exception {
        AdventParser parser = AdventParser.ofRegex("(.*) => (.*)");
        String[] charTokens = parser.getTokens("..### => #", true);

        Assert.assertEquals("..###", charTokens[0]);
        Assert.assertEquals("#", charTokens[1]);
    }

    @Test
    public void getInitialState() {
        String str = "initial state: ###....#..#..#......####.#..##..#..###......##.##..#...#.##.###.##.###.....#.###..#.#.##.#..#.#";
        AdventParser initialStateParser = AdventParser.ofRegex("initial state: (.*)");
        String[] tokens = initialStateParser.getTokens(str, true);
        int inputLength = tokens[0].length();
        int totalLength = (inputLength *2) - 1;
        BitSet state = new BitSet(totalLength);
        for (int i = inputLength-1; i < totalLength; i++) {
            char c = tokens[0].charAt(i-(inputLength-1));
            if (c == '#') {
                state.set(i, true);
            }
        }
    }


}