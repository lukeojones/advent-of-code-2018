package utils;

import javafx.util.Pair;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import static org.junit.Assert.*;

public class PatternFinderTest {

    @Test
    public void findWaveStart() {
        List<Integer> data = Arrays.asList(0, 9, 17, 1, 1, 2, 3, 4, 5, 6, 7, 8, 1, 2, 3, 4, 5, 6, 7, 8, 1, 2, 3, 4, 5, 6, 7, 8, 1);
        PatternFinder<Integer> pf = new PatternFinder<>(data);

        Pair<Integer, Integer> start = pf.findWave(1, 3);

        Assert.assertEquals(new Pair(4, 8), start);
    }
}