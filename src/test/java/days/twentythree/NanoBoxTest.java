package days.twentythree;

import org.junit.Assert;
import org.junit.Test;

import java.util.HashSet;
import java.util.Optional;

import static org.junit.Assert.*;

public class NanoBoxTest {
    @Test
    public void getIntersection() throws Exception {
        Coord open1 = new Coord(0, 0, 0);
        Coord close1 = new Coord(4, 4, 4);

        Coord open2 = new Coord(2, 2, 2);
        Coord close2 = new Coord(6, 6, 6);

        NanoBox nanoBox1 = new NanoBox(open1, close1, new HashSet<>());
        NanoBox nanoBox2 = new NanoBox(open2, close2, new HashSet<>());
        NanoBox expected = new NanoBox(open2, close1, new HashSet<>());

        Optional<NanoBox> intersection = nanoBox1.getIntersection(nanoBox2);
        Assert.assertTrue(intersection.isPresent());

        NanoBox nanoBox = intersection.get();
        Assert.assertTrue(nanoBox.same(expected));
    }

    @Test
    public void getIntersection2() throws Exception {
        Coord open1 = new Coord(0, 0, 0);
        Coord close1 = new Coord(4, 4, 4);

        Coord open2 = new Coord(10, 10, 10);
        Coord close2 = new Coord(15, 15, 15);

        NanoBox nanoBox1 = new NanoBox(open1, close1, null);
        NanoBox nanoBox2 = new NanoBox(open2, close2, null);
        NanoBox expected = new NanoBox(open2, close1, null);

        Optional<NanoBox> intersection = nanoBox1.getIntersection(nanoBox2);
        Assert.assertFalse(intersection.isPresent());
    }

    @Test
    public void getIntersection3() throws Exception {
        Coord open1 = new Coord(0, 0, 0);
        Coord close1 = new Coord(4, 4, 4);

        Coord open2 = new Coord(0, 0, 10);
        Coord close2 = new Coord(4, 4, 14);

        NanoBox nanoBox1 = new NanoBox(open1, close1, null);
        NanoBox nanoBox2 = new NanoBox(open2, close2, null);
        NanoBox expected = new NanoBox(open2, close1, null);

        Optional<NanoBox> intersection = nanoBox1.getIntersection(nanoBox2);
        Assert.assertFalse(intersection.isPresent());
    }

    @Test
    public void getIntersection4() throws Exception {
        Coord open1 = new Coord(0, 0, 0);
        Coord close1 = new Coord(4, 4, 4);

        Coord open2 = new Coord(0, 0, 2);
        Coord close2 = new Coord(4, 4, 14);

        NanoBox nanoBox1 = new NanoBox(open1, close1, new HashSet<>());
        NanoBox nanoBox2 = new NanoBox(open2, close2, new HashSet<>());
        NanoBox expected = new NanoBox(open2, close1, new HashSet<>());

        Optional<NanoBox> intersection = nanoBox1.getIntersection(nanoBox2);
        Assert.assertTrue(intersection.isPresent());

        NanoBox intersectionBox = intersection.get();
    }

}