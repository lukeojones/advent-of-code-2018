package days.twentyfour;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;

public class GroupTest {

    String t0 = "weak to radiation";
    String t1 = "immune to radiation, fire";
    String t2 = "immune to cold; weak to fire";
    String t3 = "immune to slashing";
    String t4 = "immune to bludgeoning";
    String t5 = "weak to bludgeoning, cold";
    String t7 = "weak to fire";
    String t8 = "immune to cold, slashing";
    String t9 = "weak to slashing, cold";
    String t10 = "immune to radiation";
    String t11 = "immune to slashing, fire, bludgeoning";
    String t12 = "immune to bludgeoning";
    String t13 = "weak to slashing";
    String t14 = "weak to slashing; immune to cold, bludgeoning";

    @Test
    public void parseTest1() throws Exception {
        List<Group.Power> imms = Group.parseImmunities(t1);
        List<Group.Power> weak = Group.parseWeaknesses(t1);

        Assert.assertTrue(imms.containsAll(Arrays.asList(Group.Power.RADIATION, Group.Power.FIRE)));
        Assert.assertTrue(weak.isEmpty());
    }

    @Test
    public void parseTest2() throws Exception {
        List<Group.Power> imms = Group.parseImmunities(t2);
        List<Group.Power> weak = Group.parseWeaknesses(t2);

        Assert.assertTrue(imms.containsAll(Arrays.asList(Group.Power.COLD)));
        Assert.assertTrue(weak.containsAll(Arrays.asList(Group.Power.FIRE)));
    }

    @Test
    public void parseTest11() throws Exception {
        List<Group.Power> imms = Group.parseImmunities(t11);
        List<Group.Power> weak = Group.parseWeaknesses(t11);

        Assert.assertTrue(imms.containsAll(Arrays.asList(Group.Power.SLASHING, Group.Power.BLUDGEONING, Group.Power.FIRE)));
        Assert.assertTrue(weak.isEmpty());
    }

    @Test
    public void parseTest14() throws Exception {
        List<Group.Power> imms = Group.parseImmunities(t14);
        List<Group.Power> weak = Group.parseWeaknesses(t14);

        Assert.assertTrue(imms.containsAll(Arrays.asList(Group.Power.COLD, Group.Power.BLUDGEONING)));
        Assert.assertTrue(weak.containsAll(Arrays.asList(Group.Power.SLASHING)));
    }

    @Test
    public void parseTest3() throws Exception {
        List<Group.Power> imms = Group.parseImmunities(t3);
        List<Group.Power> weak = Group.parseWeaknesses(t3);

        Assert.assertTrue(imms.containsAll(Arrays.asList(Group.Power.SLASHING)));
        Assert.assertTrue(weak.isEmpty());
    }

    @Test
    public void parseTest0() throws Exception {
        List<Group.Power> imms = Group.parseImmunities(t0);
        List<Group.Power> weak = Group.parseWeaknesses(t0);

        Assert.assertTrue(imms.isEmpty());
        Assert.assertTrue(weak.contains(Group.Power.RADIATION));
    }

}