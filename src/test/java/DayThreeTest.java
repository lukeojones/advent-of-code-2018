import org.junit.Test;

import static org.junit.Assert.*;

public class DayThreeTest {

    @Test
    public void parse() {
        String claim = "#1 @ 1,3: 4x4";
        int ix_claim_id = claim.indexOf("#");
        int ix_at = claim.indexOf("@");
        int ix_colon = claim.indexOf(":");
        String claim_id = claim.substring(ix_claim_id+1, ix_at-1);
        String co_ords = claim.substring(ix_at+2, ix_colon);
        String dims = claim.substring(ix_colon+2);

        String[] co_ord_toks = co_ords.split(",");
        String[] dim_toks = dims.split("x");

        int x = Integer.parseInt(co_ord_toks[0]);
        int y = Integer.parseInt(co_ord_toks[1]);

        int w = Integer.parseInt(dim_toks[0]);
        int h = Integer.parseInt(dim_toks[1]);
    }

}