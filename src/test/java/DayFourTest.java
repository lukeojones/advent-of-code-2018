import org.junit.Test;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static org.junit.Assert.*;

public class DayFourTest {

    private static DateTimeFormatter f = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");

    @Test
    public void test() {
        String line = "[1518-11-01 00:00] Guard #10 begins shift";
        int ixClose = line.indexOf(']');
        String dtString = line.substring(1, ixClose);
        LocalDateTime dt = LocalDateTime.from(f.parse(dtString));
    }

    @Test
    public void parseGuardId() {
        String line = "[1518-11-01 00:00] Guard #10 begins shift";

    }

}