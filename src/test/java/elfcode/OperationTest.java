package elfcode;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class OperationTest {

    @Test
    public void create() {
        Operation addr = Operation.valueOf("ADDR");

        Register[] registers = new Register[]{new Register("a", 1), new Register("b", 2)};

        int result = addr.apply(0, 1, registers);

        Assert.assertEquals(3, result);
    }

}