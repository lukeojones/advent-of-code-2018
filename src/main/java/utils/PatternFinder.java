package utils;

import javafx.util.Pair;

import java.util.*;

/**
 * Finds patterns in a list of data
 */
public class PatternFinder<T> {
    private List<T> data;

    public PatternFinder(List<T> data) {
        this.data = data;
    }

    /**
     * Find the start of a wave that has at least {@code tolerance} different matches and goes on for at least {@code cycles}
     * @param tolerance
     * @param cycles
     * @return index of wave start
     */
    public Pair<Integer,Integer> findWave(int tolerance, int cycles) {
        Map<T, Integer> dataIndices = new HashMap<>();
        for (int i=0; i<data.size(); i++) {
            T item = data.get(i);

            //item is already seen -> get index and calculate wavelength
            Integer prevIndex = dataIndices.getOrDefault(item, i);
            if (prevIndex < i) {
                //We've seen this one before
                int waveLength = i - prevIndex;

                boolean hasEnoughCycles = true;
                for (int c=2; c<=cycles; c++) { //start at two because we've already got one cycle
                    int indexNextOccurence = i + (c - 1) * waveLength;
                    if (indexNextOccurence >= data.size()) {
                        hasEnoughCycles = false;
                        break;
                    } else if (!data.get(indexNextOccurence).equals(item)) {
                        hasEnoughCycles = false;
                        break;
                    }
                }

                if (hasEnoughCycles && --tolerance == 0) {
                    return new Pair(prevIndex, waveLength);
                } else {
                    dataIndices.put(item, i);
                }
            } else {
                dataIndices.put(item, i);
            }
        }

        return null;
    }

    /**
     * Guesses the value at a given index based on the pattern identified in this wave form
     * @param index
     * @return
     */
    public T guessValueAt(int index) {
        Pair<Integer, Integer> wave = findWave(1, 4);
        int wavelength = wave.getValue();
        int waveStart = wave.getKey();

        int newIndex = ((index-waveStart) % wavelength) + waveStart;
        return data.get(newIndex);
    }
}
