package utils;

import lombok.Getter;
import lombok.Setter;

import java.util.*;

@Getter
@Setter

/**
 * Typed Implementation of Graph to use with Dkistra on Shortest Distance problems
 */
public class Graph<T> {

    private Set<GraphNode<T>> nodes = new HashSet<>();

    public void addNode(GraphNode<T> node) {
        nodes.add(node);
    }

    public static <T> Graph<T> calculateOptimalPathsFromOrigin(Graph<T> graph, GraphNode<T> origin) {
        origin.setDistance(0);

        Set<GraphNode<T>> settledNodes = new HashSet<>();
        Set<GraphNode<T>> unsettledNodes = new HashSet<>();

        unsettledNodes.add(origin);

        while (unsettledNodes.size() != 0) {
            GraphNode<T> currentNode = getNearestNode(unsettledNodes);
            unsettledNodes.remove(currentNode);

            for (Map.Entry<GraphNode<T>, Integer> e : currentNode.getNeighborsWithDist().entrySet()) {
                GraphNode neighbor = e.getKey();
                Integer dist = e.getValue();
                if (!settledNodes.contains(neighbor)) {
                    calculateMinimumDistance(neighbor, dist, currentNode);
                    unsettledNodes.add(neighbor);
                }
            }
            settledNodes.add(currentNode);
        }
        return graph;
    }

    private static <T> GraphNode<T> getNearestNode(Set<GraphNode<T>> unsettledNodes) {
        GraphNode nearestNode = null;
        int minDistance = Integer.MAX_VALUE;
        for (GraphNode node : unsettledNodes) {
            int nodeDistance = node.getDistance();
            if (nodeDistance < minDistance) {
                minDistance = nodeDistance;
                nearestNode = node;
            }
        }
        return nearestNode;
    }

    private static <T> void calculateMinimumDistance(GraphNode<T> dest,
                                                 Integer distance, GraphNode<T> src) {
        Integer sourceDistance = src.getDistance();
        int pathDistance = sourceDistance + distance;
        if (pathDistance < dest.getDistance()) {
            dest.setDistance(pathDistance);
            List<GraphNode> shortestPath = new ArrayList<>(src.getPath());
            shortestPath.add(src);
            dest.setPath(shortestPath);
        }
    }
}

//1042 is too low
