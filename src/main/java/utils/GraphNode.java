package utils;

import lombok.Getter;
import lombok.Setter;

import java.util.*;

@Getter
@Setter
public class GraphNode<T> {

    private T value;

    private List<GraphNode> path = new ArrayList<>();

    private int distance = Integer.MAX_VALUE;

    Map<GraphNode<T>, Integer> neighborsWithDist = new HashMap<>();

    public GraphNode(T value) {
        this.value = value;
    }

    public Set<Map.Entry<GraphNode<T>, Integer>> getNeighbors() {
        return neighborsWithDist.entrySet();
    }

    public void addNeighbor(GraphNode<T> node, int distance) {
        neighborsWithDist.put(node, distance);
    }

    @Override
    public String toString() {
        return value.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GraphNode<?> graphNode = (GraphNode<?>) o;
        return value.equals(graphNode.value);
    }
}
