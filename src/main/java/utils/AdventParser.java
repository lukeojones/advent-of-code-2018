package utils;

import org.apache.commons.lang3.StringUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Bunch of helpful parsing routines
 */
public class AdventParser {

    protected static final String GROUP = "(.*)";

    private final String regex;
    private final int nGroups;
    private final Pattern pattern;

    private AdventParser(String regex, Pattern pattern, int nGroups) {
        this.regex = regex;
        this.pattern = pattern;
        this.nGroups = nGroups;
    }

    /**
     * Simplifies getting a number of groups out of provided regex expression
     * @param regex
     * @return AdventParser for regex
     */
    public static AdventParser ofRegex(String regex) {
        Pattern pattern = Pattern.compile(regex);
        int nGroups = StringUtils.countMatches(regex, GROUP);
        return new AdventParser(regex, pattern, nGroups);
    }

    public static AdventParser ofCustomRegex(String regex, String group) {
        Pattern pattern = Pattern.compile(regex);
        int nGroups = StringUtils.countMatches(regex, group);
        return new AdventParser(regex, pattern, nGroups);
    }

    /**
     * Return an array of strings using the given Regex Parser
     * @return
     */
    public String[] getTokens(String input, boolean trim) {
        String[] tokens = new String[nGroups];
        Matcher matcher = pattern.matcher(input);
        if (matcher.find()) {
            for (int i=0; i<nGroups; i++) {
                String group = trim ? matcher.group(i + 1).trim() : matcher.group(i + 1);
                tokens[i] = group;
            }
        }
        return tokens;
    }

    /**
     * Return an array of Characters using the given Regex Parser
     * @return
     */
    public Character[] getCharTokens(String input) {
        Character[] tokens = new Character[nGroups];
        Matcher matcher = pattern.matcher(input);
        if (matcher.find()) {
            for (int i=0; i<nGroups; i++) {
                String group = matcher.group(i + 1).trim();
                tokens[i] = group.charAt(0);
            }
        }
        return tokens;
    }



    /**
     * Return an array of integers using the given Regex Parser
     */
    public int[] getNumericTokens(String input) {
        int[] tokens = new int[nGroups];
        Matcher matcher = pattern.matcher(input);
        if (matcher.find()) {
            for (int i=0; i<nGroups; i++) {
                tokens[i] = Integer.parseInt(matcher.group(i + 1).trim());
            }
        }
        return tokens;
    }

    public boolean matches(String input) {
        Matcher matcher = pattern.matcher(input);
        return matcher.matches();
    }
}

//====================================================================================
//        The Sum of Its Parts solution (a)
//        Answer:	ABGKCMVWYDEHFOPQUILSTNZRJX
//        Time:		110ms
//====================================================================================
//
//=====================================================================================
//        The Sum of Its Parts solution (b)
//        Answer:	898
//        Time:		7ms
//=====================================================================================
