package elfcode;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Register {
    private String label;
    private int content;

    public Register(String label, int content) {
        this.label = label;
        this.content = content;
    }
}
