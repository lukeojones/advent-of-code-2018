package elfcode;

public enum Operation {
    ADDI {
        @Override
        public int apply(int a, int b, Register[] registers){
            return registers[a].getContent() + b;
        }
    },
    ADDR {
        @Override
        public int apply(int a, int b, Register[] registers){
            return registers[a].getContent() + registers[b].getContent();
        }
    },
    MULI {
        @Override
        public int apply(int a, int b, Register[] registers){
            return registers[a].getContent() * b;
        }
    },
    MULR {
        @Override
        public int apply(int a, int b, Register[] registers){
            return registers[a].getContent() * registers[b].getContent();
        }
    },
    BANI {
        @Override
        public int apply(int a, int b, Register[] registers){
            return registers[a].getContent() & b;
        }
    },
    BANR {
        @Override
        public int apply(int a, int b, Register[] registers){
            return registers[a].getContent() & registers[b].getContent();
        }
    },
    BORI {
        @Override
        public int apply(int a, int b, Register[] registers){
            return registers[a].getContent() | b;
        }
    },
    BORR {
        @Override
        public int apply(int a, int b, Register[] registers){
            return registers[a].getContent() | registers[b].getContent();
        }
    },
    SETI {
        @Override
        public int apply(int a, int b, Register[] registers){
            return a;
        }
    },
    SETR {
        @Override
        public int apply(int a, int b, Register[] registers){
            return registers[a].getContent();
        }
    },
    GTIR {
        @Override
        public int apply(int a, int b, Register[] registers){
            return a > registers[b].getContent() ? 1 : 0;
        }
    },
    GTRI {
        @Override
        public int apply(int a, int b, Register[] registers){
            return registers[a].getContent() > b ? 1 : 0;
        }
    },
    GTRR {
        @Override
        public int apply(int a, int b, Register[] registers){
            return registers[a].getContent() > registers[b].getContent() ? 1 : 0;
        }
    },
    EQIR {
        @Override
        public int apply(int a, int b, Register[] registers){
            return a == registers[b].getContent() ? 1 : 0;
        }
    },
    EQRI {
        @Override
        public int apply(int a, int b, Register[] registers){
            return registers[a].getContent() == b ? 1 : 0;
        }
    },
    EQRR {
        @Override
        public int apply(int a, int b, Register[] registers){
            return registers[a].getContent() == registers[b].getContent() ? 1 : 0;
        }
    };


    public abstract int apply(int a, int b, Register[] registers);
}
