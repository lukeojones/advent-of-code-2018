package elfcode;

import lombok.AllArgsConstructor;
import lombok.Getter;
import utils.AdventParser;

@AllArgsConstructor
public class Instruction {
    //private static final AdventParser INSTRC = AdventParser.ofCustomRegex("(\\d+) (\\d+) (\\d+) (\\d+)", "(\\d+)");
    private static AdventParser PARSER = AdventParser.ofRegex("(.*) (.*) (.*) (.*)");

    private Operation op;
    private int arg1;
    private int arg2;

    @Getter
    private int store;

//    private Instruction(String code, int arg1, int arg2, int arg3) {
//        this.op = Operation.valueOf(code);
//        this.arg1 = arg1;
//        this.arg2 = arg2;
//        this.arg3 = arg3;
//    }

    public static Instruction of(String instruction) {
        String[] tokens = PARSER.getTokens(instruction, true);
        String code = tokens[0].toUpperCase();
        int arg1 = Integer.valueOf(tokens[1]);
        int arg2 = Integer.valueOf(tokens[2]);
        int store = Integer.valueOf(tokens[3]);
        return new Instruction(Operation.valueOf(code), arg1, arg2, store);
    }

    public int execute(Register[] registers) {
        return op.apply(arg1, arg2, registers);
    }
}

