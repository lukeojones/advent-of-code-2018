import days.twentyfour.DayTwentyFour;

public class SolutionRunner {

    public static void main(String[] args) {
        DayTwentyFour day = new DayTwentyFour(false);
        //day.solveAndLogFirst();
        day.solveAndLogSecond();
    }
}

//9891 too high
//9878
//9891
//9000 to low

//10952 too low
//10954