package days;

import javafx.util.Pair;
import utils.AdventParser;

import java.util.*;
import java.util.List;

public class DaySix extends DayOfAdvent<Integer, Integer> {

    AdventParser parser = AdventParser.ofRegex("(.*),(.*)");

    public DaySix(boolean testMode) {
        super("six", testMode);
    }

    @Override
    public Integer solveFirst() {
        int min_x = Integer.MAX_VALUE;
        int max_x = Integer.MIN_VALUE;
        int min_y = Integer.MAX_VALUE;
        int max_y = Integer.MIN_VALUE;

        List<String> coords = getFileLinesList();
        List<Pair<Integer,Integer>> points = new ArrayList<>();
        for (int i=0; i<coords.size(); i++) {
            Pair<Integer, Integer> point = parsePoint(coords.get(i));
            points.add(point);

            min_x = Math.min(point.getKey(), min_x);
            min_y = Math.min(point.getValue(), min_y);
            max_x = Math.max(point.getKey(), max_x);
            max_y = Math.max(point.getValue(), max_y);
        }

        int width =  (max_x - min_x) + 1;
        int height = (max_y - min_y) + 1;

        int[][] grid = createGrid(width, height, points, min_x, min_y);

        //Now count the most popular square excluding points that have x=max x/min x, y=max y/min y
        Map<Integer, Integer> pointLabelToCount = createPointCount(width, height, grid);
        return getMaxArea(pointLabelToCount, points, max_x, max_y, min_x, min_y);
    }

    @Override
    protected String getName() {
        return "Chronal Coordinates";
    }

    private Map<Integer, Integer> createPointCount(int width, int height, int[][] grid) {
        Map<Integer, Integer> pointLabelToCount = new HashMap<>();
        for (int i=0; i<width; i++) {
            for (int j=0;j<height;j++) {
                int closestPoint = grid[i][j];
                if (closestPoint > -1) {
                    Integer countForPoint = pointLabelToCount.get(closestPoint);
                    if (countForPoint == null) {
                        pointLabelToCount.put(closestPoint, 1);
                    } else {
                        pointLabelToCount.put(closestPoint, countForPoint + 1);
                    }
                }
            }
        }

        return pointLabelToCount;
    }

    private Pair<Integer, Integer> parsePoint(String str) {
        int[] coords = parser.getNumericTokens(str);
        return new Pair<>(coords[0], coords[1]);
    }

    private int[][] createGrid(int width, int height, List<Pair<Integer,Integer>> points, int min_x, int min_y) {
        int[][] grid = new int[width][height];
        for (int i=0; i<width; i++) {
            for (int j=0;j<height;j++) {
                int closestPoint = getClosestPoint(points, i+min_x, j+min_y);
                grid[i][j] = closestPoint;
            }
        }
        return grid;
    }

    private int getMaxArea(Map<Integer, Integer> pointLabelToCount, List<Pair<Integer,Integer>> points, int max_x, int max_y, int min_x, int min_y) {
        Iterator<Integer> iterator = pointLabelToCount.keySet().iterator();
        int maxArea = Integer.MIN_VALUE;
        while (iterator.hasNext()) {
            int pointId = iterator.next();
            Pair<Integer, Integer> point = points.get(pointId);
            if (excludePoint(point, max_x, max_y)
                    || excludePoint(point, min_x, min_y)) {
                continue;
            }

            Integer area = pointLabelToCount.get(pointId);
            if (area > maxArea) {
                maxArea = area;
            }
        }

        return maxArea;
    }

    private boolean excludePoint(Pair<Integer, Integer> point, int x_limit, int y_limit) {
        if (point.getKey() == x_limit) {
            return true;
        }

        if (point.getValue() == y_limit) {
            return true;
        }

        return false;
    }

    /**
     * Gets the closest points index
     * @param points
     * @return
     */
    private int getClosestPoint(List<Pair<Integer, Integer>> points, int x, int y) {
        int closestPoint = -1;
        int smallestDist = Integer.MAX_VALUE;
        for (int i = 0; i < points.size(); i++) {
            Pair<Integer, Integer> point = points.get(i);
            int dist = Math.abs(x - point.getKey()) + Math.abs(y - point.getValue());
            if (dist < smallestDist) {
                smallestDist = dist;
                closestPoint = i;
            } else if (dist == smallestDist) {
                closestPoint = -2;
            }
        }

        return closestPoint;
    }


    @Override
    protected Integer solveSecond() {
        int min_x = Integer.MAX_VALUE;
        int max_x = Integer.MIN_VALUE;
        int min_y = Integer.MAX_VALUE;
        int max_y = Integer.MIN_VALUE;

        List<Pair<Integer,Integer>> points = new ArrayList<>();
        List<String> coords = getFileLinesList();
        for (int i=0; i<coords.size(); i++) {
            String str = coords.get(i);
            Pair<Integer, Integer> point = parsePoint(str);
            points.add(point);

            min_x = Math.min(min_x, point.getKey());
            max_x = Math.max(max_x, point.getKey());
            min_y = Math.min(min_y, point.getValue());
            max_y = Math.max(max_y, point.getValue());
        }

        //Now make map with extra border
        int width = max_x-min_x+2;
        int height = max_y-min_y+1;
        int safeCount = 0;
        for (int i=0; i<width; i++) {
            for (int j=0;j<height;j++) {
                int isSafe = isSafe(points, i+min_x, j+min_y);
                safeCount = safeCount + isSafe;
            }
        }

        return safeCount;
    }

    /**
     * Returns true if this point is less than total of 10000 from all points
     * @param points
     * @return
     */
    private int isSafe(List<Pair<Integer, Integer>> points, int x, int y) {
        int totalDist = 0;
        for (int i = 0; i < points.size(); i++) {
            Pair<Integer, Integer> point = points.get(i);
            int dist = Math.abs(x - point.getKey()) + Math.abs(y - point.getValue());
            totalDist = totalDist + dist;
        }

        if (totalDist < 10000) {
            return 1;
        }

        return 0;
    }

//====================================================================================
//    Chronal Coordinates solution (a)
//    Answer:	4819 (Puzzle Six was broken and accepted 4342 instead)
//    Time:		167ms
//====================================================================================
//
//=====================================================================================
//    Chronal Coordinates solution (b)
//    Answer:	42966
//    Time:		52ms
//=====================================================================================


//Stuff to draw the map! Not part of question
//    private void drawGrid(int[][] grid) {
//        int width = grid.length;
//        int height = grid[0].length;
//
//        BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
//        for (int i = 0; i< width; i++) {
//            for (int j = 0; j< height; j++) {
//                int value = grid[i][j];
//                if (value < 0) {
//                    value = Color.WHITE.getRGB();
//                } else {
//                    value = getNiceColor(value);
//                }
//                image.setRGB(i, j, value);
//            }
//        }
//
//        File op = new File("map.png");
//        try {
//            ImageIO.write(image, "png", op);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }
//
//    List<Integer> niceColors = null;
//
//    private int getNiceColor(int i) {
//        if (niceColors == null) {
//            niceColors = getNiceColors();
//        }
//        return niceColors.get(i % niceColors.size());
//    }
//
//    private List<Integer> getNiceColors() {
//        List<Integer> niceColors = new ArrayList<>();
//        niceColors.add(Integer.parseInt("8B0000",16));
//        niceColors.add(Integer.parseInt("A52A2A",16));
//        niceColors.add(Integer.parseInt("B22222",16));
//        niceColors.add(Integer.parseInt("DC143C",16));
//        niceColors.add(Integer.parseInt("FF0000",16));
//        niceColors.add(Integer.parseInt("FF6347",16));
//        niceColors.add(Integer.parseInt("FF7F50",16));
//        niceColors.add(Integer.parseInt("CD5C5C",16));
//        niceColors.add(Integer.parseInt("F08080",16));
//        niceColors.add(Integer.parseInt("E9967A",16));
//        niceColors.add(Integer.parseInt("FA8072",16));
//        niceColors.add(Integer.parseInt("FFA07A",16));
//        niceColors.add(Integer.parseInt("FF4500",16));
//        niceColors.add(Integer.parseInt("8B0000",16));
//        niceColors.add(Integer.parseInt("A52A2A",16));
//        niceColors.add(Integer.parseInt("B22222",16));
//        niceColors.add(Integer.parseInt("DC143C",16));
//        niceColors.add(Integer.parseInt("FF0000",16));
//        niceColors.add(Integer.parseInt("FF6347",16));
//        niceColors.add(Integer.parseInt("FF7F50",16));
//        niceColors.add(Integer.parseInt("CD5C5C",16));
//        niceColors.add(Integer.parseInt("F08080",16));
//        niceColors.add(Integer.parseInt("E9967A",16));
//        niceColors.add(Integer.parseInt("FA8072",16));
//        niceColors.add(Integer.parseInt("FFA07A",16));
//        niceColors.add(Integer.parseInt("FF4500",16));
//        niceColors.add(Integer.parseInt("B0E0E6",16));
//        niceColors.add(Integer.parseInt("5F9EA0",16));
//        niceColors.add(Integer.parseInt("4682B4",16));
//        niceColors.add(Integer.parseInt("6495ED",16));
//        niceColors.add(Integer.parseInt("00BFFF",16));
//        niceColors.add(Integer.parseInt("1E90FF",16));
//        niceColors.add(Integer.parseInt("ADD8E6",16));
//        niceColors.add(Integer.parseInt("87CEEB",16));
//        niceColors.add(Integer.parseInt("87CEFA",16));
//        niceColors.add(Integer.parseInt("00FF00",16));
//        niceColors.add(Integer.parseInt("32CD32",16));
//        niceColors.add(Integer.parseInt("90EE90",16));
//        niceColors.add(Integer.parseInt("98FB98",16));
//        niceColors.add(Integer.parseInt("8FBC8F",16));
//        niceColors.add(Integer.parseInt("00FA9A",16));
//        niceColors.add(Integer.parseInt("00FF7F",16));
//        niceColors.add(Integer.parseInt("2E8B57",16));
//        niceColors.add(Integer.parseInt("66CDAA",16));
//        niceColors.add(Integer.parseInt("3CB371",16));
//        niceColors.add(Integer.parseInt("20B2AA",16));
//        niceColors.add(Integer.parseInt("2F4F4F",16));
//        niceColors.add(Integer.parseInt("008080",16));
//        niceColors.add(Integer.parseInt("008B8B",16));
//        niceColors.add(Integer.parseInt("00FFFF",16));
//        return niceColors;
//    }
}
