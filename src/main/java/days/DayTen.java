package days;

import utils.AdventParser;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class DayTen extends DayOfAdvent<String, Integer> {

    private static int TIME = 40000;

    private AdventParser parser = AdventParser.ofRegex("position=<(.*),(.*)> velocity=<(.*),(.*)>");
    private int convergenceTimeAnswer;

    public DayTen(boolean testMode) {
        super("ten", testMode);
    }

    public static class Light {
        private int xorig;
        private int yorig;
        private int x;
        private int y;
        private int xVelocity;
        private int yVelocity;

        public Light(int xpos, int ypos, int xvel, int yvel) {
            this.x = xpos;
            this.y = ypos;
            this.xorig = xpos;
            this.yorig = ypos;
            this.xVelocity = xvel;
            this.yVelocity = yvel;
        }

        /**
         * Move the light in an incremental fashion
         */
        protected void move() {
            x += xVelocity;
            y += yVelocity;
        }

        /**
         * Move the light to the position it would be in at particular time
         * @param time
         */
        protected void moveToTime(int time) {
            x = xorig + xVelocity*time;
            y = yorig + yVelocity*time;
        }
    }

    @Override
    protected String solveFirst() {
        List<String> lines = getFileLinesList();

        List<Light> lights = new ArrayList<>();
        for (String line:lines) {
            int[] tokens = parser.getNumericTokens(line);
            Light light = new Light(tokens[0], tokens[1], tokens[2], tokens[3]);
            lights.add(light);
        }

        convergenceTimeAnswer = findConvergenceTime(lights);
        final int convergenceTime = convergenceTimeAnswer;
        lights.stream()
                .forEach(light -> light.moveToTime(convergenceTime));

        return draw(lights);
    }

    @Override
    protected Integer solveSecond() {
        return convergenceTimeAnswer;
    }

    @Override
    protected String getName() {
        return "The Stars Align";
    }

    /**
     * Find the time at which the lights fit inside their smallest horizontal stripe
     * @param lights
     * @return
     */
    private int findConvergenceTime(List<Light> lights) {
        int minHeight = Integer.MAX_VALUE;
        int convergenceTime = TIME;
        for (int i=0; i<TIME; i++) {
            if (i > 0) {
                lights.stream()
                        .forEach(Light :: move);
            }
            int height = findHeight(lights);
            if (height < minHeight) {
                minHeight = height;
                convergenceTime = i;
            }
        }

        System.out.println("Convergence time: " + convergenceTime);
        return convergenceTime;
    }

    private int findHeight(List<Light> lights) {
        int[] dimensions = getMinMaxDimensions(lights);
        return dimensions[3] - dimensions[2];
    }

    private String draw(List<Light> lights) {
        int[] bounds = getMinMaxDimensions(lights);
        int minX = bounds[0];
        int maxX = bounds[1];
        int minY = bounds[2];
        int maxY = bounds[3];

        int width = maxX - minX;
        int height = maxY - minY;
        BufferedImage image = new BufferedImage(width+10, height+10, BufferedImage.TYPE_INT_RGB);


        for (int i=0; i<lights.size(); i++) {
            Light light = lights.get(i);
            image.setRGB(light.x - minX, light.y - minY, Color.WHITE.getRGB());
        }

        return writeToFile(image);
    }

    /**
     * Returns the minimum X, maximum X, minimum Y and maximum Y co-ords of a point
     * @param lights
     * @return
     */
    private int[] getMinMaxDimensions(List<Light> lights) {
        int minX = Integer.MAX_VALUE;
        int maxX = Integer.MIN_VALUE;
        int minY = Integer.MAX_VALUE;
        int maxY = Integer.MIN_VALUE;

        for (Light light :  lights) {
            minY = Math.min(minY, light.y);
            minX = Math.min(minX, light.x);
            maxY = Math.max(maxY, light.y);
            maxX = Math.max(maxX, light.x);
        }

        return new int[]{minX, maxX, minY, maxY};
    }

    private String writeToFile(BufferedImage image) {
        File op = new File("lights.png");
        try {
            ImageIO.write(image, "png", op);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return op.getAbsolutePath();
    }
}

//====================================================================================
//        The Stars Align solution (a)
//        Answer:	/Users/luke/Dev/advent-of-code-2018/lights.png
//        Time:	901ms
//====================================================================================
//
//=====================================================================================
//        The Stars Align solution (b)
//        Answer:	10886
//        Time:	0ms
//=====================================================================================
