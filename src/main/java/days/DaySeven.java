package days;

import utils.AdventParser;

import java.util.*;

public class DaySeven extends DayOfAdvent<String, Integer> {

    AdventParser parser = AdventParser.ofRegex("Step (.*) must be finished before step (.*) can begin.");

    public DaySeven(boolean testMode) {
        super("seven", testMode);
    }

    @Override
    public Integer solveSecond() {
            Map<Character, List<Character>> actionsAndDependencies = getActionDependencyMap();

            //Now iterate through the map generating the order of instructions
            List<Character> actionsPerformed = new ArrayList<>();
            Map<Character, Integer> taskToFinishTime = new HashMap<>();

            int time = 0;
            while (!actionsAndDependencies.isEmpty()) {

                if (!haveAvailableWorker(taskToFinishTime)) {
                    time = updateWorkTimeTable(time, taskToFinishTime, actionsPerformed);
                    continue;
                }

                //Sort the actions alphabetically
                ArrayList<Character> actions = new ArrayList(actionsAndDependencies.keySet());
                Collections.sort(actions);

                //Find one with no dependencies
                for (int i=0; i<actions.size(); i++) {
                    Character action = actions.get(i);
                    List<Character> dependenciesForAction = actionsAndDependencies.get(action);
                    if (dependenciesForAction == null
                            || dependenciesForAction.isEmpty()
                            || allDependenciesDone(dependenciesForAction, actionsPerformed)) {
                        addActionToWorkTable(action, taskToFinishTime, time);
                        actionsAndDependencies.remove(action);

                        //Only stop looking for tasks if we have no more availability
                        if (!haveAvailableWorker(taskToFinishTime)) {
                            break;
                        }
                    }
                }

                time = updateWorkTimeTable(time, taskToFinishTime, actionsPerformed);
            }

        return time;
    }

    private int updateWorkTimeTable(int time, Map<Character, Integer> taskToFinishTime, List<Character> actionsPerformed) {
        ArrayList<Character> tasks = new ArrayList(taskToFinishTime.keySet());

        int nextFinishTime = Integer.MAX_VALUE;
        for (Character task:tasks) {
            Integer finishTime = taskToFinishTime.get(task);

            //if a task has finished, remove from map
            if (time == finishTime) {
                actionsPerformed.add(task);
                taskToFinishTime.remove(task);
            }

            if (finishTime < nextFinishTime) {
                nextFinishTime = finishTime;
            }
        }

        //return the next time something has happened
        return nextFinishTime;
    }

    private boolean haveAvailableWorker(Map<Character, Integer> taskToFinishTime) {
        return taskToFinishTime.size() < 5;
    }

    private void addActionToWorkTable(Character action, Map<Character, Integer> taskToFinishTime, int currentTime) {
        taskToFinishTime.put(action, action - 4 + currentTime);
    }

    private Map<Character, List<Character>> getActionDependencyMap() {
        Map<Character, List<Character>> actionsAndDependencies = new HashMap<>();
        List<Character> dependencies = new ArrayList<>();

        List<String> steps = getFileLinesList();
        for (int i = 0; i<steps.size(); i++) {
            Character[] depAndAction = parser.getCharTokens(steps.get(i));
            Character dependency = depAndAction[0];
            Character action = depAndAction[1];

            dependencies.add(dependency);

            List<Character> actionDependencies = actionsAndDependencies.get(action);
            if (actionDependencies == null) {
                actionDependencies = new ArrayList<>();
                actionsAndDependencies.put(action, actionDependencies);
            }
            actionDependencies.add(dependency);
        }

        //Make sure dependencies are in my map (not super efficient but N is small)
        for (int i=0; i<dependencies.size(); i++) {
            Character dependency = dependencies.get(i);

            List<Character> characters = actionsAndDependencies.get(dependency);
            if (characters == null) {
                characters = new ArrayList<>();
                actionsAndDependencies.put(dependency, characters);
            }
        }

        return actionsAndDependencies;
    }

    @Override
    public String solveFirst() {

        Map<Character, List<Character>> actionsAndDependencies = getActionDependencyMap();

        //Now iterate through the map generating the order of instructions
        List<Character> actionsPerformed = new ArrayList<>();

        while (!actionsAndDependencies.isEmpty()) {

            //Sort the actions alphabetically
            ArrayList<Character> actions = new ArrayList(actionsAndDependencies.keySet());
            Collections.sort(actions);

            //Find one with no dependencies
            for (int i=0; i<actions.size(); i++) {
                Character action = actions.get(i);
                List<Character> dependenciesForAction = actionsAndDependencies.get(action);
                if (dependenciesForAction == null
                        || dependenciesForAction.isEmpty()
                        || allDependenciesDone(dependenciesForAction, actionsPerformed)) {
                    actionsPerformed.add(action);
                    actionsAndDependencies.remove(action);
                    break;
                }
            }
        }

        String ans = "";
        for (int s = 0; s < actionsPerformed.size(); s++) {
            Character character = actionsPerformed.get(s);
            ans = ans + character;
        }

        return ans;
    }

    private boolean allDependenciesDone(List<Character> dependenciesForAction, List<Character> actionsPerformed) {
        for (int i=0; i<dependenciesForAction.size(); i++) {
            Character dependency = dependenciesForAction.get(i);
            if (!actionsPerformed.contains(dependency)) {
                return false;
            }
        }
        return true;
    }

    @Override
    protected String getName() {
        return "The Sum of Its Parts";
    }
}
