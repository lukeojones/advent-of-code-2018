package days;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DayNine extends DayOfAdvent<Long, Long> {

    private List<Player> players;
    private int lastMarble;

    //Puzzle Inputs
    private static List<Integer> TEST_1 = Arrays.asList(9, 25, 32);
    private static List<Integer> TEST_2 = Arrays.asList(10, 1618, 8317);
    private static List<Integer> TEST_3 = Arrays.asList(13, 7999, 146373);
    private static List<Integer> TEST_4 = Arrays.asList(17, 1104, 2764);
    private static List<Integer> TEST_5 = Arrays.asList(21, 6111, 54718);
    private static List<Integer> TEST_6 = Arrays.asList(30, 5807, 37305);
    private static List<Integer> REAL_A = Arrays.asList(471, 72026, -1);
    private static List<Integer> REAL_B = Arrays.asList(471, 7202600, -1);

    public DayNine(boolean test) {
        super("nine", test);
    }

    @Override
    public Long solveFirst() {
        configure(REAL_A);
        return solve();
    }

    @Override
    public Long solveSecond() {
        configure(REAL_B);
        return solve();
    }

    @Override
    protected String getName() {
        return "Marble Mania";
    }

    public static class Marble {
        private Marble cw;
        private Marble acw;
        private long value;

        public Marble(long value) {
            this.value = value;
        }
    }

    public static class Player {
        private int id = 0;
        private long score = 0;

        public Player(int id) {
            this.id = id;
        }
    }

    protected void configure(List<Integer> parameters) {

        //Create list of the players of this game
        List<Player> players = new ArrayList<>();
        for (int i=1; i<=parameters.get(0); i++) {
            players.add(new Player(i));
        }

        //Configure the parameters of this test
        this.players = players;
        this.lastMarble = parameters.get(1);
    }

    protected Player nextPlayer(int round) {
        return players.get((round - 1) % players.size());
    }

    protected long solve() {
        //Add the first marble
        Marble head = new Marble(0);
        head.cw = head;
        head.acw = head;

        //Now add the rest
        for (int i=1; i<=lastMarble; i++) {
            Player player = nextPlayer(i);

            Marble marble = new Marble(i);
            if (marble.value % 23 == 0) {

                //Add the marbles value to players score
                player.score += marble.value;

                //Move seven places ACW
                Marble sevenShift = head.acw;
                for (int j=0;j<6; j++) {
                    sevenShift = sevenShift.acw;
                }

                //Add the value of this marble to players score
                player.score += sevenShift.value;

                //Move the head to the removed marble cw marble
                head = sevenShift.cw;

                //Remove the marble

                //Store next and prev pointers
                Marble prev = sevenShift.acw;
                Marble next = sevenShift.cw;

                //Joint next and prev together
                prev.cw = next;
                next.acw = prev;

            } else {
                Marble singleShift = head.cw;
                Marble doubleShift = singleShift.cw;

                //repoint existing marbles
                singleShift.cw = marble;
                doubleShift.acw = marble;

                //join the new marble
                marble.acw = singleShift;
                marble.cw = doubleShift;

                //current marble is the placed marble
                head = marble;
            }
        }

        long maxScore = Integer.MIN_VALUE;
        for (Player elf : players) {
            if (elf.score > maxScore) {
                maxScore = elf.score;
            }
        }

        return maxScore;
    }
}

//====================================================================================
//        Marble Mania solution (a)
//        Answer:	390093
//        Time:		23ms
//====================================================================================
//
//=====================================================================================
//        Marble Mania solution (b)
//        Answer:	3150377341
//        Time:		2309ms
//=====================================================================================
