package days.twentythree;

public class NanoBot {

    private final Coord loc;
    private final int r;
    private final int id;

    public NanoBot(Coord loc, int r, int id) {
        this.loc = loc;
        this.r = r;
        this.id = id;
    }

    public boolean inRange(NanoBot bot) {
        return loc.getDistanceFrom(bot.getLoc()) <= r;
    }

    public boolean inRange(Coord other) {
        return other.getDistanceFrom(loc) <= r;
    }

    public Coord getLoc() {
        return loc;
    }

    /**
     * Zoom out from this NanoBot by a factor of {@code factor}.
     * The ceiling of the radius division is used so we don't end up missing intersections due to rounding.
     * @param factor
     * @return new NanoBot as if the view has been zoomed out by some factor
     */
    public NanoBot zoomOut(int factor) {
        return new NanoBot(loc.zoomOut(factor), (int)Math.ceil((double)r/factor), id);
    }
}
