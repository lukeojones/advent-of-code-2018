package days.twentythree;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class Coord {
    private final int x;
    private final int y;
    private final int z;

    public int getDistanceFrom(Coord loc) {
        return Math.abs(x - loc.getX()) + Math.abs(y - loc.getY()) + Math.abs(z - loc.getZ());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Coord coord = (Coord) o;

        if (x != coord.x) return false;
        if (y != coord.y) return false;
        return z == coord.z;
    }

    @Override
    public int hashCode() {
        int result = x;
        result = 31 * result + y;
        result = 31 * result + z;
        return result;
    }

    public Coord zoomOut(int factor) {
        return new Coord(x/factor, y/factor, z/factor);
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getZ() {
        return z;
    }
}
