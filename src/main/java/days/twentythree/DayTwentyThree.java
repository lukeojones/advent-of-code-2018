package days.twentythree;

import days.DayOfAdvent;
import javafx.util.Pair;
import utils.AdventParser;

import java.util.*;
import java.util.stream.Collectors;

public class DayTwentyThree extends DayOfAdvent<Integer, Integer> {

    private AdventParser parser = AdventParser.ofRegex("pos=<(.*),(.*),(.*)>, r=(.*)");
    private List<NanoBot> bots = null;
    private List<NanoBot> originalBots = null;
    private Coord maxlimit;
    private Coord minlimit;
    private int xmax = Integer.MIN_VALUE;
    private int ymax = Integer.MIN_VALUE;
    private int zmax = Integer.MIN_VALUE;
    private int xmin = Integer.MAX_VALUE;
    private int ymin = Integer.MAX_VALUE;
    private int zmin = Integer.MAX_VALUE;

    private Map<NanoBox, Set<NanoBot>> botMap = new HashMap<>();

    public DayTwentyThree(boolean testMode) {
        super("twentythree", testMode);
    }

    @Override
    protected Integer solveFirst() {
        List<String> lines = getFileLinesList();
        bots = new ArrayList<>(lines.size());
        originalBots = new ArrayList<>(lines.size());
        NanoBot strongest = null;

        int rmax = Integer.MIN_VALUE;
        for (int i = 0; i < lines.size(); i++) {
            int[] toks = parser.getNumericTokens(lines.get(i));

            int x = toks[0];
            int y = toks[1];
            int z = toks[2];
            int r = toks[3];

            Coord loc = new Coord(x, y, z);
            NanoBot bot = new NanoBot(loc, r, i);
            NanoBot originalBot = new NanoBot(loc, r, i);
            if (r > rmax) {
                rmax = r;
                strongest = bot;
            }

            ymax = Math.max(ymax, y+r);
            xmax = Math.max(xmax, x+r);
            zmax = Math.max(zmax, z+r);
            ymin = Math.min(ymin, y-r);
            xmin = Math.min(xmin, x-r);
            zmin = Math.min(zmin, z-r);

            bots.add(bot);
            originalBots.add(originalBot);
        }

        minlimit = new Coord(xmin, ymin, zmin);
        maxlimit = new Coord(xmax, ymax, zmax);

        int inRange = 0;
        for (NanoBot bot : bots) {
            if (strongest.inRange(bot)) {
                inRange++;
            }
        }

        return inRange;
    }

    @Override
    protected Integer solveSecond() {

        System.out.println("X Range: " + xmin + " --> " + xmax);
        System.out.println("Y Range: " + ymin + " --> " + ymax);
        System.out.println("Z Range: " + zmin + " --> " + zmax);


        Coord min = minlimit;
        Coord max = maxlimit;

        int factor = 1048576;
        Coord[] optimal = null;
        while (factor >=1) {
            optimal = getOptimalLocation(factor, min, max);
            min = optimal[0];
            max = optimal[1];
            factor = factor / 2;
        }

        System.out.println("Min Point is : " + optimal[0].getX() + "," + optimal[0].getY() + "," + optimal[0].getZ());
        System.out.println("Max Point is : " + optimal[1].getX() + "," + optimal[1].getY() + "," + optimal[1].getZ());

        int[] answers = getBotCount(optimal[0], optimal[1]);
        System.out.println("Bot Count: " + answers[1]);
        return answers[0];
    }

    private Coord[] getOptimalLocation(int factor, Coord min, Coord max) {
        List<NanoBot> zoomBots = originalBots.stream().map(b -> b.zoomOut(factor)).collect(Collectors.toList());
        Coord minScaled = min.zoomOut(factor);
        Coord maxScaled = max.zoomOut(factor);

        System.out.println("X Range: " + minScaled.getX() + " --> " + maxScaled.getX());
        System.out.println("Y Range: " + minScaled.getY() + " --> " + maxScaled.getY());
        System.out.println("Z Range: " + minScaled.getZ() + " --> " + maxScaled.getZ());

        int maxCount = 0;
        Coord optimal = null;

        for (int y=minScaled.getY()-1; y<=maxScaled.getY()+1; y++) {
            for (int x=minScaled.getX()-1; x<=maxScaled.getX()+1; x++) {
                for (int z=minScaled.getZ()-1; z<=maxScaled.getZ()+1; z++) {
                    Coord point = new Coord(x, y, z);
                    int botCount = 0;
                    for (NanoBot bot: zoomBots) {
                        if (bot.inRange(point)) {
                            botCount++;
                        }
                    }

                    if (botCount > maxCount) {
                        maxCount = botCount;
                        optimal = point;
                    }
                }
            }
        }

        Coord newMin = new Coord((optimal.getX()-1)*factor, (optimal.getY()-1)*factor, (optimal.getZ()-1)*factor);
        Coord newMax = new Coord((optimal.getX()+1)*factor, (optimal.getY()+1)*factor, (optimal.getZ()+1)*factor);
        return new Coord[]{newMin, newMax};
    }

    private int[] getBotCount(Coord min, Coord max) {
        final int BUFFER = 10;
        int maxBotCount = 0;
        Coord maxpoint = null;
        for (int y=min.getY()-BUFFER; y<=max.getY()+BUFFER; y++) {
            for (int z=min.getZ()-BUFFER; z<=max.getZ()+BUFFER; z++) {
                for (int x=min.getX()-BUFFER; x<=max.getX()+BUFFER; x++) {
                    int botCount = 0;

                    Coord point = new Coord(x, y, z);
                    for (NanoBot bot: originalBots) {
                        if (bot.inRange(point)) {
                            botCount++;
                        }
                    }

                    if (botCount > maxBotCount) {
                        maxBotCount = botCount;
                        maxpoint = point;
                    }
                }
            }
        }

        System.out.println("Max Point is : " + maxpoint.getX() + "," + maxpoint.getY() + "," + maxpoint.getZ());
        int distance = maxpoint.getX() + maxpoint.getY() + maxpoint.getZ();
        System.out.println("Distance is: " + distance);
        return new int[]{distance, maxBotCount};
    }

    @Override
    protected String getName() {
        return "Experimental Emergency Teleportation";
    }
}
