package days.twentythree;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Getter
@Setter
@AllArgsConstructor
public class NanoBox {
    private Coord a;
    private Coord b;
    private Set<NanoBot> contents = new HashSet<>();

    public Optional<NanoBox> getIntersection(NanoBox other) {
        NanoBox maxByX = other;
        NanoBox minByX = this;

        if (other.getMinX() < getMinX()) {
            minByX = other;
            maxByX = this;
        }

        if (minByX.getMaxX() < maxByX.getMinX()) {
            return Optional.empty();
        }

        NanoBox maxByY = other;
        NanoBox minByY = this;

        if (other.getMinY() < getMinY()) {
            minByY = other;
            maxByY = this;
        }

        if (minByY.getMaxY() < maxByY.getMinY()) {
            return Optional.empty();
        }

        NanoBox maxByZ = other;
        NanoBox minByZ = this;

        if (other.getMinZ() < getMinZ()) {
            minByZ = other;
            maxByZ = this;
        }

        if (minByZ.getMaxZ() < maxByZ.getMinZ()) {
            return Optional.empty();
        }

        int xopen = minByX.getMaxX();
        int xclose = maxByX.getMinX();

        int yopen = minByY.getMaxY();
        int yclose = maxByY.getMinY();

        int zopen = minByZ.getMaxZ();
        int zclose = maxByZ.getMinZ();

        Coord open = new Coord(xopen, yopen, zopen);
        Coord close = new Coord(xclose, yclose, zclose);
        Set<NanoBot> contents = new HashSet(getContents());
        contents.addAll(other.getContents());
        return Optional.of(new NanoBox(open, close, contents));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        NanoBox nanoBox = (NanoBox) o;

//        if (a != null ? !a.equals(nanoBox.a) : nanoBox.a != null) return false;
//        return b != null ? b.equals(nanoBox.b) : nanoBox.b == null;

        if (a.equals(nanoBox.getA()) && this.b.equals(nanoBox.getB())) {
            return true; //direct match
        }

        return a.equals(nanoBox.getB()) && this.b.equals(nanoBox.getA());
    }

    @Override
    public int hashCode() {
        int result = a != null ? a.hashCode() : 0;
        result += b != null ? b.hashCode() : 0;
        return result;
    }

    public boolean same(NanoBox other) {
        if (a == null) {
            return false;
        }

        if (b == null) {
            return false;
        }

        if (a.equals(other.getA()) && this.b.equals(other.getB())) {
            return true; //direct match
        }

        return a.equals(other.getB()) && this.b.equals(other.getA());
    }

    public int getMinX() {
        return Math.min(a.getX(), b.getX());
    }

    public int getMaxX() {
        return Math.max(a.getX(), b.getX());
    }

    public int getMinY() {
        return Math.min(a.getY(), b.getY());
    }

    public int getMaxY() {
        return Math.max(a.getY(), b.getY());
    }

    public int getMinZ() {
        return Math.min(a.getZ(), b.getZ());
    }

    public int getMaxZ() {
        return Math.max(a.getZ(), b.getZ());
    }
}
