package days.eighteen;

import days.DayOfAdvent;
import javafx.util.Pair;
import utils.AdventParser;
import utils.PatternFinder;

import java.util.ArrayList;
import java.util.List;

public class DayEighteen extends DayOfAdvent<Long, Long> {

    private static final int BORDER = 1;
    private static final int BUFFER = BORDER * 2;
    private static int TIME = 10;
    private List<String> trendData = new ArrayList<>();

    public DayEighteen(boolean testMode) {
        super("eighteen", testMode);
    }

    @Override
    protected Long solveFirst() {
        trendData.clear();
        Acre[][] acres = parseGrid();

        System.out.println("Initial Grid");
        //printGrid(acres);

        return passTime(acres);
    }

    private long countValue(Acre[][] acres) {
        long countLumber = 0;
        long countTrees = 0;
        for (int y = 1;y < acres.length-BORDER; y++) {
            for (int x = 1; x < acres[0].length-BORDER; x++) {
                Acre acre = acres[y][x];
                if (acre.isLumberYard()) {
                    countLumber++;
                } else if (acre.isTrees()) {
                    countTrees++;
                }
            }
        }

        trendData.add("Trees: " + countTrees + " Lumber: " + countLumber);
        return countLumber * countTrees;
    }

    private long passTime(Acre[][] acres) {
        long value = 0;
        for (int t=1;t<=TIME;t++) {
            Acre[][] copy = deepCopy(acres);
            passMinute(copy, acres);
            value = countValue(acres);
            //System.out.println();
        }
        return value;
    }

    private void passMinute(Acre[][] copy, Acre[][] grid) {
        for (int y = 1;y < grid.length-BORDER; y++) {
            for (int x = 1; x < grid[0].length-BORDER; x++) {
                grid[y][x].evolve(copy, grid);
            }
        }
    }

    private Acre[][] deepCopy(Acre[][] grid) {
        Acre[][] copy = new Acre[grid.length][grid[0].length];
        for (int y=0;y<grid.length;y++) {
            for (int x = 0; x < grid[0].length; x++) {
                copy[y][x] = Acre.copy(grid[y][x]);;
            }
        }
        return copy;
    }

    private Acre[][] parseGrid() {
        List<String> lines = getFileLinesList();
        int height = lines.size() + BUFFER;
        int width = lines.size() + BUFFER;

        Acre[][] grid = new Acre[height][width];

        for (int y=0;y<height;y++) {
            for (int x = 0; x < width; x++) {
                //Create the hz borders
                Acre acre = null;
                if (y == 0 || y == height - 1) {
                    acre = Acre.border(x, y);
                } else if (x == 0 || x == width - 1) {
                    acre = Acre.border(x, y);
                } else {
                    String line = lines.get(y - 1);
                    acre = Acre.of(x, y, line.charAt(x - 1));
                }
                grid[y][x] = acre;
            }
        }

        return grid;
    }

    private void printGrid(Acre[][] grid) {
        for(int i = 0; i<grid.length; i++)
        {
            for(int j = 0; j<grid[0].length; j++)
            {
                System.out.print(grid[i][j]);
            }
            System.out.println();
        }
    }

    @Override
    protected Long solveSecond() {
        TIME = 1000;
        solveFirst();

        PatternFinder<String> pf = new PatternFinder<>(trendData);
        Pair<Integer, Integer> wave = pf.findWave(1, 4);
        System.out.println("Wave of length: "  + wave.getValue() + " starts at " + wave.getKey());

        String val = pf.guessValueAt(1000_000_000-1);
        System.out.println("Guessed value: " + val);

        int[] toks = AdventParser.ofRegex("Trees: (.*) Lumber: (.*)").getNumericTokens(val);
        return (long)toks[0] * toks[1];
    }

    @Override
    protected String getName() {
        return "Settlers of The North Pole";
    }
}
