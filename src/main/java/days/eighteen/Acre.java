package days.eighteen;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.awt.*;
import java.util.HashMap;
import java.util.Map;

@Getter
@AllArgsConstructor
public class Acre {
    public static char LUMBERYARD = '#';
    public static char TREES = '|';
    public static char OPEN = '.';
    public static char BORDER = 'x';
    private static int BOX_SIZE = 3;

    private char type;
    private Point pt;

    public static Acre border(int x, int y) {
        return new Acre(BORDER, new Point(x, y));
    }

    public static Acre of(int x, int y, char c) {
        return new Acre(c, new Point(x, y));
    }

    public static Acre copy(Acre acre) {
        return new Acre(acre.type, acre.pt);
    }

    @Override
    public String toString() {
        return "" + type;
    }

    //Plug in the rules
    public void evolve(Acre[][] copy, Acre[][] grid) {
        Map<Character, Integer> counts = count(copy);
        if (isOpen()) {
            if (counts.getOrDefault(TREES, 0) >= 3) {
                type = TREES;
            }
        } else if (isTrees()) {
            if (counts.getOrDefault(LUMBERYARD, 0) >= 3) {
                type = LUMBERYARD;
            }
        } else if (isLumberYard()) {
            int trees = counts.getOrDefault(TREES, 0);
            int yards = counts.getOrDefault(LUMBERYARD, 0);
            if (trees >= 1 && yards >= 1) {
                type = LUMBERYARD;
            } else {
                type = OPEN;
            }
        }
    }

    public boolean isLumberYard() {
        return type == LUMBERYARD;
    }

    public boolean isTrees() {
        return type == TREES;
    }

    private boolean isOpen() {
        return type == OPEN;
    }

    private Map<Character, Integer> count(Acre[][] copy) {
        Map<Character, Integer> typeCount = new HashMap<>();

        int bk = (BOX_SIZE-1) / 2;
        int fw = (BOX_SIZE-1) / 2;

        int y1 = pt.y - bk;
        int y2 = pt.y + fw;

        int x1 = pt.x - bk;
        int x2 = pt.x + fw;

        for (int y=y1;y<=y2;y++) {
            for (int x=x1;x<=x2;x++) {

                //Miss the middle point
                if (x != pt.x || y != pt.y) {
                    Acre adj = copy[y][x];
                    Integer count = typeCount.getOrDefault(adj.type, 0);
                    typeCount.put(adj.type, count+1);
                }
            }
        }

        return typeCount;
    }
}
