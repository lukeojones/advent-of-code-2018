package days.twentyone;

import days.DayOfAdvent;
import elfcode.Instruction;
import elfcode.Register;
import utils.AdventParser;

import java.util.*;
import java.util.stream.Collectors;

public class DayTwentyOne extends DayOfAdvent<Integer, Integer> {

    private AdventParser parser = AdventParser.ofRegex("(.*) (.*) (.*) (.*)");
    public DayTwentyOne(boolean testMode) {
        super("twentyone", testMode);
    }
    private int answerOne = -1;
    private int answerTwo = -1;

    @Override
    protected Integer solveFirst() {
        run();
        return answerOne;
    }

    @Override
    protected Integer solveSecond() {
        return answerTwo;
    }

    private Integer run() {
        Register[] registers = new Register[]{
                new Register("r0", 0),
                new Register("r1", 0),
                new Register("r2", 0),
                new Register("r3", 0),
                new Register("r4", 0),
                new Register("r5", 0)};

        List<String> sourceCode = getFileLinesList();
        int ipReg = AdventParser.ofRegex("#ip (.*)").getNumericTokens(sourceCode.remove(0))[0];
        Register regIP = registers[ipReg];
        regIP.setLabel("ip");
        System.out.println("Setting IP Register to: " + ipReg);

        List<Instruction> instructions = sourceCode.stream()
                .map(Instruction::of)
                .collect(Collectors.toList());

        int pointer = 0;

        Set<Integer> haltingValueSet = new HashSet<>();
        while (pointer < instructions.size()) {

            //Store pointer into ip reg
            regIP.setContent(pointer);

            //Execute the instruction and store
            Instruction instruction = instructions.get(pointer);
            int result = instruction.execute(registers);

            Register store = registers[instruction.getStore()];
            store.setContent(result);

            int oldPointer = pointer;

            //Write ipReg content back to pointer
            pointer = regIP.getContent();

            //Increment pointer
            if (oldPointer == 28 && registers[3].getContent() != 0) {
                if (!addUnique(haltingValueSet, registers[3].getContent())) {
                    return answerOne;
                }
            }

            pointer++;
        }

        return answerOne;
    }

    private boolean addUnique(Set<Integer> haltingValueSet, Integer val) {
        if (haltingValueSet.contains(val)) {
            return false;
        }

        haltingValueSet.add(val);

        if (answerOne == -1) {
            answerOne = val;
        }
        answerTwo = val;

        return true;
    }

    @Override
    protected String getName() {
        return "Chronal Conversion";
    }

//====================================================================================
//    Chronal Conversion solution (a)
//    Answer:	9107763
//    Time:		34309ms
//====================================================================================
//
//=====================================================================================
//    Chronal Conversion solution (b)
//    Answer:	7877093
//    Time:		0ms
//=====================================================================================

//    private void runProgram2() {
//        int reg0 = 0;
//        int reg1 = 0;
//        int reg2 = 0;
//        int reg3 = 0;
//        int reg4 = 0;
//        int reg5 = 0;
//
//        Set<Integer> haltingVals = new HashSet<>();
//
//        while (true) {
//            reg4 = reg3 | 65536;
//            reg3 = 7041048;
//            while (true) {
//                reg5 = reg4 & 255;
//                reg3 = reg3 + reg5;
//                reg3 = reg3 & 16777215;
//                reg3 = reg3 * 65899;
//                reg3 = reg3 & 16777215;
//
//                if (256 > reg4) {
//                    if (!haltingVals.contains(reg3)) {
//                        haltingVals.add(reg3);
//                        System.out.println(reg3);
//                    }
//
//                    break;
//                }
//
//                reg5 = 0;
//                reg1 = reg5 + 1;
//                reg1 = reg1 * 256;
//                if (reg1 > reg4) {
//
//                }
//            }
//        }
//    }
}
