package days.fifteen;

import java.util.ArrayDeque;
import java.util.Queue;

public class PathFinderSimple
{
    private static final int MAX_ITERATIONS = 100000;

    public PathFinderSimple(Cell[][] grid, Cell source) {
        this.height = grid.length;
        this.width = grid[0].length;
        this.grid = grid;
        this.visited = new boolean[height][width];
        this.source = source;
    }

    private int height;
    private int width;
    private Cell[][] grid;
    private boolean[][] visited;
    private Cell source;
    int min_dist = Integer.MAX_VALUE;
    PathFinderNode bestNode = null;
    //private Cell target;

    // Below arrays details all 4 possible movements from a cell
    private static final int row[] = { -1, 0, 0, 1 };
    private static final int col[] = { 0, -1, 1, 0 };

    // Function to check if it is possible to go to position (row, col)
    // from current position. The function returns false if (row, col)
    // is not a valid position or has value 0 or it is already visited
    private boolean isValid(int row, int col)
    {
        return (row >= 0) && (row < height) && (col >= 0) && (col < width)
                && grid[row][col].isAvailable() && !visited[row][col];
    }

    // Find Shortest Possible Route in a matrix mat from source
    // cell (i, j) to destination cell (x, y)
    public PathFinderNode solve(Cell target)
    {
        // construct a matrix to keep track of visited cells
        boolean[][] visited = new boolean[height][width];

        // create an empty queue
        Queue<PathFinderNode> q = new ArrayDeque<>();

        // mark source cell as visited and enqueue the source node
        int y = source.getY();
        int x = source.getX();
        visited[y][x] = true;
        q.add(new PathFinderNode(x, y, 0, null));

        // stores length of shortest path from source to destination
        //int min_dist = Integer.MAX_VALUE;

        int iterations = 0;

        // run till queue is not empty
        PathFinderNode node = null;
        while (!q.isEmpty())
        {
            // pop front node from queue and process it
            node = q.poll();

            // (i, j) represents current cell and dist stores its
            // minimum distance from the source
            x = node.getX();
            y = node.getY();
            int dist = node.getDist();

            if (dist > min_dist) {
               break;
            }

            // if destination is found, update min_dist and stop
            if (x == target.getX() && y == target.getY())
            {
                min_dist = dist;
                bestNode = node;
                break;
            }

            // check for all 4 possible movements from current cell
            // and enqueue each valid movement
            if (source.getX() == 7 && source.getY() == 3) {
                //System.out.println("Moving out from " + x + "," + y);
            }

            for (int dir = 0; dir < 4; dir++)
            {
                // check if it is possible to go to position
                // (i + row[k], j + col[k]) from current position
                if (isValid(y + row[dir], x + col[dir]))
                {
                    // mark next cell as visited and enqueue it
                    visited[y + row[dir]][x + col[dir]] = true;
                    q.add(new PathFinderNode(x + col[dir], y + row[dir], dist + 1, node));
                }
            }

            if (iterations > MAX_ITERATIONS) {
                //System.out.println("Exceeded max path finder iterations - cannot trust o/p");
                //throw new IllegalStateException();
                break;
            }

            //printMatrix(visited);
            iterations++;
        }

        if (min_dist != Integer.MAX_VALUE) {
            return bestNode;
        }
        else {
            return null;
        }
    }

    private static <T> void printMatrix(boolean[][] matrix) {
        for(int i = 0; i<matrix.length; i++)
        {
            for(int j = 0; j<matrix[0].length; j++)
            {
                System.out.print(matrix[i][j]);
            }
            System.out.println();
        }
    }
}