package days.fifteen;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@Getter
@Setter
public class Cell {

    public static final char WALL = '#';
    public static final char SPACE = '.';

    private int x;
    private int y;
    private char type;
    private Unit unit;
    private int nodeNum;

    public Cell(int x, int y, char type, int nodeNum) {
        this.x = x;
        this.y = y;
        this.type = type;
        this.unit = Unit.of(type);
        this.nodeNum = nodeNum;
    }

    public boolean isWall() {
        return type == WALL;
    }

    public boolean isSpace() {
        return type == SPACE;
    }

    public boolean isUnit() {
        return unit != null;
    }

    @Override
    public String toString() {
        return "" + type;
    }

    public String desc() {
        return "" + type + " @ " + getX() + "," + getY();
    }

    public boolean nextToTarget(Cell[][] grid, char enemyRace) {
        return      grid[y][x-1].type == enemyRace
                ||  grid[y][x+1].type == enemyRace
                ||  grid[y-1][x].type == enemyRace
                ||  grid[y+1][x].type == enemyRace;
    }

    public boolean isAvailable() {
        return isSpace();
    }

    public List<Cell> getEnemyCells(Cell[][] grid) {
        List<Cell> enemies = new ArrayList<>();
        Cell north = north(grid);
        Cell west = west(grid);
        Cell east = east(grid);
        Cell south = south(grid);

        if (north.hasEnemyOfUnit(unit)) {
            enemies.add(north);
        }

        if (west.hasEnemyOfUnit(unit)) {
            enemies.add(west);
        }

        if (east.hasEnemyOfUnit(unit)) {
            enemies.add(east);
        }

        if (south.hasEnemyOfUnit(unit)) {
            enemies.add(south);
        }

        Collections.sort(enemies, new EnemyComparator());
        return enemies;
    }

    public void suffer() {
        unit.suffer();
        if (unit.getHp() <= 0) {
            unit = null;
            type = Cell.SPACE;
        }
    }

    private Cell north(Cell[][] grid) {
        return grid[getY()-1][getX()];
    }

    private Cell south(Cell[][] grid) {
        return grid[getY()+1][getX()];
    }

    private Cell east(Cell[][] grid) {
        return grid[getY()][getX()+1];
    }

    private Cell west(Cell[][] grid) {
        return grid[getY()][getX()-1];
    }

    public boolean hasEnemyOfUnit(Unit unit) {
        Unit otherUnit = getUnit();
        if (otherUnit == null) {
            return false;
        }

        return otherUnit.getEnemyRace() == unit.getRace();
    }

    public static class ReadComparator implements Comparator<Cell> {
        @Override
        public int compare(Cell o1, Cell o2) {
            if (o1.getY() == o2.getY()) {
                return o1.getX() - o2.getX();
            }
            return o1.getY() - o2.getY();
        }
    }

    public static class EnemyComparator implements Comparator<Cell> {

        @Override
        public int compare(Cell o1, Cell o2) {
            if (o1.getUnit().getHp() == o2.getUnit().getHp()) {
                if (o1.getY() == o2.getY()) {
                    return o1.getX() - o2.getX();
                }

                return o1.getY() - o2.getY();
            }

            return o1.getUnit().getHp() - o2.getUnit().getHp();
        }
    }
}
