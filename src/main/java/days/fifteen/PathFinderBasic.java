package days.fifteen;

import java.util.*;

public class PathFinderBasic {

    private Cell[][] grid;
    private Cell source;
    private Cell target;
    private int height;
    private int width;
    private boolean[][] visited;
    private int minDistance = Integer.MAX_VALUE;
    private PathNode lastNode = null;

    private Map<PathNode, Integer> cache = new HashMap<>();

    Queue<PathNode> queue = new LinkedList<>();

    public PathFinderBasic(Cell[][] grid, Cell source) {
        this.grid = grid;
        this.height = grid.length;
        this.width = grid[0].length;
        this.source = source;
    }

    public static class PathNode {
        private List<PathNode> children = new ArrayList<>();
        private Cell cell;
        private int distance;
        private PathNode prev;

        public Cell getCell() {
            return cell;
        }

        public PathNode(Cell cell, PathNode prev, int distance) {
            this.children = children;
            this.cell = cell;
            this.prev = prev;
            this.distance = distance;
        }

        public void addChild(PathNode child) {
            children.add(child);
        }

        public List<PathNode> getForwardPath() {
            List<PathNode> path = new ArrayList<>();

            PathNode head = this;
            while (head.prev != null) {
                path.add(0, head);
                head = head.prev;
            }
            return path;
        }
    }

    public PathNode search(Cell target) {
        if (source == null) {
            return null;
        }

        visited = new boolean[height][width];
        queue.clear();
        queue.add(new PathNode(source, null, 0));

        while (!queue.isEmpty()) {
            PathNode node = queue.remove();

            int y = node.cell.getY();
            int x = node.cell.getX();

            //We're already processing these in read order so we only want to
            //update the path if the distance is genuinely less.
            if (node.distance >= minDistance) {
                break;
            }

            if (y == target.getY() && x == target.getX()) {
                minDistance = node.distance;
                lastNode = node;
                break;
            }

            Cell north = grid[y-1][x];
            Cell west = grid[y][x-1];
            Cell east = grid[y][x+1];
            Cell south = grid[y+1][x];

            if (north.isAvailable() && !visited[north.getY()][north.getX()]) {
                visited[north.getY()][north.getX()] = true;
                PathNode child = new PathNode(north, node, node.distance + 1);
                node.addChild(child);
                queue.add(child);
            }

            if (west.isAvailable() && !visited[west.getY()][west.getX()]) {
                visited[west.getY()][west.getX()] = true;
                PathNode child = new PathNode(west, node, node.distance + 1);
                node.addChild(child);
                queue.add(child);
            }

            if (east.isAvailable() && !visited[east.getY()][east.getX()]) {
                visited[east.getY()][east.getX()] = true;;
                PathNode child = new PathNode(east, node, node.distance + 1);
                node.addChild(child);
                queue.add(child);
            }

            if (south.isAvailable() && !visited[south.getY()][south.getX()]) {
                visited[south.getY()][south.getX()] = true;
                PathNode child = new PathNode(south, node, node.distance + 1);
                node.addChild(child);
                queue.add(child);
            }
        }

        return lastNode;
    }
}
