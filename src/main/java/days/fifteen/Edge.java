package days.fifteen;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;

@Getter
@AllArgsConstructor
public class Edge {
    private int fromNodeIndex;
    private int toNodeIndex;
    private int length;

    public int getNeighbourIndex(int nodeIndex) {
        if (this.fromNodeIndex == nodeIndex) {
            return this.toNodeIndex;
        } else {
            return this.fromNodeIndex;
        }
    }

    public static Edge[] buildEdges(Cell[][] grid) {
        HashSet<Edge> forward = new HashSet<>();
        HashSet<Edge> reverse = new HashSet<>();
        for (int y=0;y<grid.length; y++) {
            for (int x=0; x<grid[0].length; x++) {
                Cell cell = grid[y][x];
                if (cell.isAvailable()) {
                    Cell north = grid[y-1][x];
                    Cell west = grid[y][x-1];
                    Cell east = grid[y][x+1];
                    Cell south = grid[y+1][x];

                    createEdge(cell, north, forward, reverse);
                    createEdge(cell, west, forward, reverse);
                    createEdge(cell, east, forward, reverse);
                    createEdge(cell, south, forward, reverse);
                }
            }
        }

        return new ArrayList<>(forward).toArray(new Edge[]{});
    }

    private static void createEdge(Cell from, Cell to, HashSet<Edge> forward, HashSet<Edge> reverse) {
        if (to.isAvailable()) {
            Edge edge = new Edge(from.getNodeNum(), to.getNodeNum(), 1);
            if (!reverse.contains(edge)) {
                forward.add(edge);
                reverse.add(new Edge(edge.getToNodeIndex(), edge.getFromNodeIndex(), 1));
            }
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Edge edge = (Edge) o;
        return fromNodeIndex == edge.fromNodeIndex &&
                toNodeIndex == edge.toNodeIndex &&
                length == edge.length;
    }

    @Override
    public int hashCode() {
        return Objects.hash(fromNodeIndex, toNodeIndex, length);
    }
}
