package days.fifteen;

import java.util.*;

public class PathFinder {

    private final static int TRIED = 2;
    private final static int PATH = 3;

    private Cell[][] grid;
    private Unit unit;
    private Cell targetCell;
    private int height;
    private int width;
    //private int route;

    private int[][] map;
    private List<Cell> route = new ArrayList<>();

    public PathFinder(Cell[][] grid, Unit unit, Cell targetCell) {
        this.grid = grid;
        this.unit = unit;
        this.targetCell = targetCell;
        this.height = grid.length;
        this.width = grid[0].length;
        this.map = new int[height][width];
    }

//    public boolean solve() {
//        boolean traverse = traverse(unit.getCell().getY(), unit.getCell().getX());
//        System.out.println(this.toString());
//        System.out.println("Step count: " + route.size() + " - soln: " + traverse);
//        return traverse;
//    }
//
//    private boolean traverse(int i, int j) {
//        if (!isValid(i, j)) {
//            return false;
//        }
//
//        if (isEnd(i, j)) {
//            //map[i][j] = PATH;
//            addStep(i, j);
//            return true;
//        } else {
//            map[i][j] = TRIED;
//        }
//
//        // North
//        if (traverse(i - 1, j)) {
//            //map[i - 1][j] = PATH;
//            addStep(i-1, j);
//            return true;
//        }
//        // East
//        if (traverse(i, j + 1)) {
//            //map[i][j + 1] = PATH;
//            addStep(i, j+1);
//            return true;
//        }
//        // South
//        if (traverse(i + 1, j)) {
//            //map[i + 1][j] = PATH;
//            addStep(i+1, j);
//            return true;
//        }
//        // West
//        if (traverse(i, j - 1)) {
//            //map[i][j - 1] = PATH;
//            addStep(i, j-1);
//            return true;
//        }
//
//        return false;
//    }
//
//    private void addStep(int i, int j) {
//        if (map[i][j] != PATH) {
//            map[i][j] = PATH;
//            System.out.println("Adding step " + j + "," + i);
//            route.add(0, new Cell(j, i, 'R'));
//        }
//    }
//
//    public List<Cell> getRoute() {
//        return route;
//    }
//
//    public int getRouteLength() {
//        return route.size();
//    }
////
////    private boolean isOrigin(int i, int j) {
////        return i == unit.getCell().getY()
////                && j == unit.getCell().getX();
////    }
//
//    private boolean isEnd(int i, int j) {
//        return i == targetCell.getY()
//                && j == targetCell.getX();
//    }
//
//    private boolean isValid(int i, int j) {
//        if (inRange(i, j) && isOpen(i, j) && !isTried(i, j)) {
//            return true;
//        }
//
//        return false;
//    }
//
//    private boolean isOpen(int i, int j) {
//        return grid[i][j].isSpace()
//                || ((grid[i][j].getX() == unit.getCell().getX()) && ((grid[i][j].getY() == unit.getCell().getY())));
//    }
//
//    private boolean isTried(int i, int j) {
//        return map[i][j] == TRIED;
//    }
//
//    private boolean inRange(int i, int j) {
//        return inHeight(i) && inWidth(j);
//    }
//
//    private boolean inHeight(int i) {
//        return i >= 0 && i < height;
//    }
//
//    private boolean inWidth(int j) {
//        return j >= 0 && j < width;
//    }
//
//    public String toString() {
//        String s = "";
//        for (int[] row : map) {
//            s += Arrays.toString(row) + "\n";
//        }
//
//        return s;
//    }
}
