package days.fifteen;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Getter
@Setter
@AllArgsConstructor
public class Unit {
    private static final int AP = 3;
    public static int AP_ELF = 3;
    private static final char ELF = 'E';
    private static final char GOBLIN = 'G';

    private char race;
    private int hp;

    public Unit(char race) {
        this.race = race;
        this.hp = 200;
    }

    /**
     * Goes through a grid and gets the r/o list of units
     * @param grid
     * @return
     */
    public static Set<Unit> findElves(Cell[][] grid) {
        return Arrays.stream(grid)
                .flatMap(arr -> Arrays.stream(arr))
                .filter(Cell:: isUnit)
                .map(Cell :: getUnit)
                .filter(Unit :: isElf)
                .collect(Collectors.toSet());
    }

    public static Set<Unit> findGoblins(Cell[][] grid) {
        return Arrays.stream(grid)
                .flatMap(arr -> Arrays.stream(arr))
                .filter(Cell:: isUnit)
                .map(Cell :: getUnit)
                .filter(Unit :: isGoblin)
                .collect(Collectors.toSet());
    }

    public static Unit of(char type) {
        if (type == ELF) {
            return new Unit(ELF);
        } else if (type == GOBLIN) {
            return new Unit(GOBLIN);
        }
        return null;
    }

    public List<Cell> getCellsInRangeOfEnemy(Cell[][] grid) {
        return Arrays.stream(grid)
                .flatMap(arr -> Arrays.stream(arr))
                .filter(Cell:: isSpace)
                .filter(c -> c.nextToTarget(grid, getEnemyRace()))
                .collect(Collectors.toList());
    }



    public char getEnemyRace() {
        if (isElf()) {
            return GOBLIN;
        } else if (isGoblin()) {
            return ELF;
        }

        throw new IllegalStateException("No Enemy Race defined!");
    }

    public boolean isElf() {
        return race == ELF;
    }

    public boolean isGoblin() {
        return race == GOBLIN;
    }

    public void suffer() {
        if (isGoblin()) {
            hp = hp - AP_ELF;
        } else {
            hp = hp - AP;
        }
    }
}
