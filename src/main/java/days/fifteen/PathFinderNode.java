package days.fifteen;

import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

@Getter
public class PathFinderNode
{
    private int x;
    private int y;
    private int dist;
    private PathFinderNode prev;

    PathFinderNode(int x, int y, int dist, PathFinderNode prev) {
        this.x = x;
        this.y = y;
        this.dist = dist;
        this.prev = prev;
    }

    public List<PathFinderNode> getForwardPath() {
        List<PathFinderNode> path = new ArrayList<>();

        PathFinderNode head = this;
        while (head.prev != null) {
            path.add(0, head);
            head = head.prev;
        }
        return path;
    }
}