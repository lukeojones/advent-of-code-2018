package days.fifteen;

import days.DayOfAdvent;

import java.util.*;

public class DayFifteen extends DayOfAdvent<Integer, Integer> {

    private Cell[][] grid;
    private HashSet<Unit> elves = new HashSet<>();
    private HashSet<Unit> goblins = new HashSet<>();
    private Set<Unit> winningTeam = null;
    private static int elvesLost = 0;

    public DayFifteen(boolean testMode) {
        super("fifteen", testMode);
    }

    private void reset() {
        elves = new HashSet<>();
        goblins = new HashSet<>();
        winningTeam = null;
        elvesLost = 0;
    }

    @Override
    protected Integer solveFirst() {
        grid = parseGrid();
        //printGrid(grid);

        Set<Unit> elves = Unit.findElves(grid);
        Set<Unit> goblins = Unit.findGoblins(grid);

        int iterations = 0;
        boolean atWar = true;
        while (atWar) {
            HashSet<Unit> processed = new HashSet<>();
            for (int y=0; y<grid.length; y++) {
                for (int x=0;x<grid[0].length; x++) {
                    Cell cellWithUnit = grid[y][x];
                    if (cellWithUnit.getUnit() == null) {
                        continue;
                    }

                    Unit unit = cellWithUnit.getUnit();
                    if (processed.contains(unit)) {
                        continue;
                    }

                    processed.add(unit);
                    List<Cell> enemies = cellWithUnit.getEnemyCells(grid);
                    if (!enemies.isEmpty()) {
                        //System.out.println(cellWithUnit.desc() + " is attacking " + enemies.get(0).desc());
                        Cell enemyCell = enemies.get(0);
                        Unit enemyUnit = enemyCell.getUnit();
                        enemyUnit.suffer();
                        if (enemyUnit.getHp() <= 0 ) {
                            enemyCell.setUnit(null);
                            enemyCell.setType(Cell.SPACE);
                            if (enemyUnit.isElf()) {
                                elvesLost++;
                                elves.remove(enemyUnit);
                                //return -1;
                            } else if (enemyUnit.isGoblin()) {
                                goblins.remove(enemyUnit);
                            }
                        }
                        continue;
                    }

                    //System.out.println("Processing " + cellWithUnit.desc());
                    List<Cell> cellsInRange = cellWithUnit.getUnit().getCellsInRangeOfEnemy(grid);
                    if (cellsInRange.isEmpty()) {
                        //We might be in situation where we are just hacking at the person next to us
                        //This shouldn't end combat but it will stop us moving anyone around
                        //System.out.println("No cells in range");
                        continue;
                    }

                    List<PathFinderBasic.PathNode> path = null;
                    PathFinderBasic pf = new PathFinderBasic(grid, cellWithUnit);
                    PathFinderBasic.PathNode lastNode = null;
                    for (Cell cell : cellsInRange) {
                        lastNode = pf.search(cell);
                    }

                    if (lastNode == null) {
                        continue;
                    }

                    path = lastNode.getForwardPath();
                    //Graph graph = new Graph(Edge.buildEdges(grid));

                    if (path == null) {
                        System.out.println("No paths found at iteration " + iterations);
                        continue;
                    }

                    //Move the unit to the new cell in the grid
                    Cell step = path.get(0).getCell();

                    //Unit unit = cellWithUnit.getUnit();
                    grid[step.getY()][step.getX()].setUnit(unit);
                    grid[step.getY()][step.getX()].setType(unit.getRace());

                    //Mark the original cell as a non-unit cell now
                    cellWithUnit.setType(Cell.SPACE);
                    cellWithUnit.setUnit(null);

                    Cell destinationCell = grid[step.getY()][step.getX()];
                    enemies = destinationCell.getEnemyCells(grid);
                    if (!enemies.isEmpty()) {
                        //System.out.println(destinationCell.desc() + " just bumped into " + enemies.get(0).desc() + " and attacks");
                        //printGrid(grid);
                        Cell enemy = enemies.get(0);
                        enemy.suffer();
                        //printGrid(grid);
                        continue;
                    }
                    //printGrid(grid);
                }
            }

            if (elves.isEmpty()) {
                atWar = false;
                System.out.println("Goblins win");
                winningTeam = goblins;
                break;
            } else if (goblins.isEmpty()) {
                atWar = false;
                System.out.println("Elves win");
                winningTeam = elves;
                break;
            }

            iterations++;
            System.out.println("Finished Round: " + iterations);
            printGrid(grid);
        }

        int hpSum = winningTeam.stream()
                .mapToInt(Unit::getHp)
                .sum();

        System.out.println("HpSum is: " + hpSum);
        System.out.println("Iterations is: " + iterations);

        return iterations * hpSum;
    }

    private Cell[][] parseGrid() {
        List<String> lines = getFileLinesList();

        int height = lines.size();
        int width = lines.stream()
                .max(Comparator.comparingInt(String::length))
                .get()
                .length();

        //Build a 2d array representing the grid
        int nodeNum = 0;
        Cell[][] grid = new Cell[height][width];
        for (int y=0; y<height; y++) {
            char[] chars = lines.get(y).toCharArray();
            for (int x = 0; x < width && x < chars.length; x++) {
                char c = chars[x];
                Cell cell = new Cell(x, y, c, nodeNum++);
                grid[y][x] = cell;
            }
        }

        return grid;
    }

    private void printGrid(Cell[][] grid) {
        for(int i = 0; i<grid.length; i++) {

            StringBuilder sb = new StringBuilder();
            for(int j = 0; j<grid[0].length; j++) {
                System.out.print(grid[i][j]);
                Cell cell = grid[i][j];
                Unit unit = cell.getUnit();
                if (unit != null) {
                    sb.append(unit.getRace() + "(" + unit.getHp() + "), ");
                }
            }
            System.out.print("\t\t" + sb.toString());
            System.out.println();
        }
        System.out.println();
    }

    @Override
    protected Integer solveSecond() {
        for (int i=Unit.AP_ELF-1; i<201; i++) {
            Unit.AP_ELF = Unit.AP_ELF + 1;
            System.out.println("Running for AP_ELF = " + Unit.AP_ELF);
            reset();
            Integer ans = solveFirst();
            System.out.println("Elves lost = " + elvesLost);
            if (ans > -1) {
                return ans;
            }
        }

        return null;
    }

    @Override
    protected String getName() {
        return "Beverage Bandits";
    }
}
