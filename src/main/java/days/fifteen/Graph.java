package days.fifteen;

import java.util.ArrayList;

public class Graph {
    private Node[] nodes;
    private int nNodes;
    private Edge[] edges;
    private int nEdges;

    public Graph(Edge[] edges) {
        this.edges = edges;
        this.nNodes = calculateNumberOfNodes(edges);
        this.nodes = new Node[this.nNodes];
        for (int n = 0; n < this.nNodes; n++) {
            this.nodes[n] = new Node();
        }

        // add all the edges to the nodes, each edge added to two nodes (to and from)
        this.nEdges = edges.length;
        for (int edgeToAdd = 0; edgeToAdd < this.nEdges; edgeToAdd++) {
            this.nodes[edges[edgeToAdd].getFromNodeIndex()].getEdges().add(edges[edgeToAdd]);
            this.nodes[edges[edgeToAdd].getToNodeIndex()].getEdges().add(edges[edgeToAdd]);
        }
    }

    private int calculateNumberOfNodes(Edge[] edges) {
        int nNodes = 0;
        for (Edge e : edges) {
            if (e.getToNodeIndex() > nNodes)
                nNodes = e.getToNodeIndex();
            if (e.getFromNodeIndex() > nNodes)
                nNodes = e.getFromNodeIndex();
        }
        nNodes++;
        return nNodes;
    }

    public void calculateShortestDistances() {
        this.nodes[0].setDistanceFromSource(0);
        int nextNode = 0;
        // visit every node
        for (int i = 0; i < this.nodes.length; i++) {
            // loop around the edges of current node
            ArrayList<Edge> currentNodeEdges = this.nodes[nextNode].getEdges();
            for (int joinedEdge = 0; joinedEdge < currentNodeEdges.size(); joinedEdge++) {
                int neighbourIndex = currentNodeEdges.get(joinedEdge).getNeighbourIndex(nextNode);
                // only if not visited
                if (!this.nodes[neighbourIndex].isVisited()) {
                    int tentative = this.nodes[nextNode].getDistanceFromSource() + currentNodeEdges.get(joinedEdge).getLength();
                    if (tentative < nodes[neighbourIndex].getDistanceFromSource()) {
                        nodes[neighbourIndex].setDistanceFromSource(tentative);
                    }
                }
            }
            // all neighbours checked so node visited
            nodes[nextNode].setVisited(true);
            // next node must be with shortest distance
            nextNode = getNodeShortestDistanced();
        }
    }

    private int getNodeShortestDistanced() {
        int storedNodeIndex = 0;
        int storedDist = Integer.MAX_VALUE;
        for (int i = 0; i < this.nodes.length; i++) {
            int currentDist = this.nodes[i].getDistanceFromSource();
            if (!this.nodes[i].isVisited() && currentDist < storedDist) {
                storedDist = currentDist;
                storedNodeIndex = i;
            }
        }
        return storedNodeIndex;
    }

    public void printResult() {
        String output = "Number of nodes = " + this.nNodes;
        output += "\nNumber of edges = " + this.nEdges;
        for (int i = 0; i < this.nodes.length; i++) {
            output += ("\nThe shortest distance from node 0 to node " + i + " is " + nodes[i].getDistanceFromSource());
        }
        System.out.println(output);
    }
}
