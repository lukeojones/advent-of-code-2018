package days.twenty;

import days.DayOfAdvent;
import utils.Graph;
import utils.GraphNode;

import java.awt.*;
import java.util.*;

public class DayTwenty extends DayOfAdvent<Integer, Integer> {

    Graph<Point> graph = new Graph<>();
    GraphNode<Point> root = new GraphNode<>(new Point(0,0));

    private String expr = null;

    private static final Point NORTH = new Point(0,1);
    private static final Point EAST = new Point(1,0);
    private static final Point WEST = new Point(-1,0);
    private static final Point SOUTH = new Point(0,-1);

    public DayTwenty(boolean testMode) {
        super("twenty", testMode);
    }

    @Override
    protected Integer solveFirst() {
        expr = getFileLinesList().get(0);
        expr = expr.substring(1, expr.length() - 1);
        process(expr);

        Graph.calculateOptimalPathsFromOrigin(graph, root);
        int maxDistance = 0;
        for (GraphNode<Point> node : graph.getNodes()) {
            maxDistance = Math.max(maxDistance, node.getDistance());
        }

        return maxDistance;
    }

    private void process(String expr) {
        //For tracking which rooms have been visited
        Map<Point, GraphNode<Point>> visited = new HashMap<>();

        //Add the root node to the stack
        Stack<GraphNode<Point>> stack = new Stack<>();
        stack.push(root);
        GraphNode<Point> curr = root;

        for (int i=0; i<expr.length(); i++) {
            char c = expr.charAt(i);

            Point dir = getDir(c);
            if (dir != null) {
                Point pt = new Point(curr.getValue().x + dir.x, curr.getValue().y + dir.y);

                GraphNode<Point> n = visited.get(pt);
                if (n == null) {
                    n = new GraphNode(pt);
                    visited.put(pt, n);
                    graph.addNode(n);
                }

                curr.addNeighbor(n, 1);
                curr = n;

                continue;
            }

            if (isOpen(c)) {
                stack.push(curr);
                continue;
            }

            if (isPipe(c)) {
                curr = stack.peek();
                continue;
            }

            if (isClose(c)) {
                stack.pop();
                continue;
            }
        }
    }

    private boolean isClose(char c) {
        return c == ')';
    }

    private boolean isPipe(char c) {
        return c == '|';
    }

    private boolean isOpen(char c) {
        return c == '(';
    }

    private Point getDir(char c) {
        if (isNorth(c)) {
            return NORTH;
        }

        if (isEast(c)) {
            return EAST;
        }

        if (isWest(c)) {
            return WEST;
        }

        if (isSouth(c)) {
            return SOUTH;
        }

        return null;
    }

    private boolean isNorth(char c) {
        return c == 'N';
    }

    private boolean isEast(char c) {
        return c == 'E';
    }

    private boolean isWest(char c) {
        return c == 'W';
    }

    private boolean isSouth(char c) {
        return c == 'S';
    }

    @Override
    protected Integer solveSecond() {
        int count = 0;
        for (GraphNode<Point> node : graph.getNodes()) {
            if (node.getDistance() >= 1000) {
                count++;
            }
        }

        return count;
    }

    @Override
    protected String getName() {
        return "A Regular Map";
    }
}
