package days.twentytwo;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.awt.*;
import java.util.Objects;

@AllArgsConstructor
public class State {
    private Point pt;

    @Getter
    private Gear gear;

    public int getX() {
        return pt.x;
    }

    public int getY() {
        return pt.y;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        State state = (State) o;
        return Objects.equals(pt, state.pt) &&
                gear == state.gear;
    }

    @Override
    public int hashCode() {
        return Objects.hash(pt, gear);
    }
}
