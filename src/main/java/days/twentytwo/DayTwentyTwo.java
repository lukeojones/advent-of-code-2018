package days.twentytwo;

import days.DayOfAdvent;
import utils.AdventParser;
import utils.Graph;
import utils.GraphNode;

import java.awt.*;
import java.util.*;
import java.util.List;

public class DayTwentyTwo extends DayOfAdvent<Integer, Integer> {

    private final static int BUFFER = 100;
    private final AdventParser depthParser = AdventParser.ofRegex("depth: (.*)");
    private final AdventParser targetParser = AdventParser.ofRegex("target: (.*),(.*)");
    Map<State, GraphNode<State>> visited = new HashMap<>();
    private Point target = null;
    private Cell[][] grid = null;

    public DayTwentyTwo(boolean testMode) {
        super("twentytwo", testMode);
    }

    @Override
    protected Integer solveFirst() {
        Integer depth = getDepth();
        target = getTarget();

        //Cell.DEPTH = depth;
        Cell.DEPTH = target.y + BUFFER;
        Cell.WIDTH = target.x + BUFFER;

        grid = buildGrid(depth, target);
        printGrid(grid);
        int risk = calculateRisk(grid);
        return risk;
    }

    private int calculateRisk(Cell[][] grid) {
        int risk = 0;
        for (int y=0; y<=target.y; y++) {
            for (int x=0; x<=target.x; x++) {
                risk += grid[y][x].getRisk();
            }
        }
        return risk;
    }

    private Cell[][] buildGrid(int depth, Point target) {
        int xmax = target.x + BUFFER + 1;
        //int ymax = depth + 1;
        int ymax = depth + 1;

        Cell[][] grid = new Cell[ymax][xmax];

        for (int y=0; y<ymax; y++) {
            for (int x=0; x<xmax; x++) {
                grid[y][x] = Cell.of(y, x, target, depth, grid);
            }
        }

        return grid;
    }

    private Integer getDepth() {
        return depthParser.getNumericTokens(getFileLinesList().get(0))[0];
    }

    private Point getTarget() {
        int[] toks = targetParser.getNumericTokens(getFileLinesList().get(1));
        return new Point(toks[0], toks[1]);
    }

    private void printGrid(Cell[][] grid) {
        for(int i = 0; i<grid.length; i++)
        {
            int j = 0;
            for(j = 0; j<grid[0].length; j++)
            {
                char type = grid[i][j].getType();
                if (i==0 && j==0) {
                    type = 'M';
                } else if (i==target.y && j==target.x) {
                    type = 'T';
                }
                System.out.print(type);
            }
            System.out.println();
        }
    }

    @Override
    protected Integer solveSecond() {
        Graph<State> graph = new Graph<>();

        GraphNode<State> root = new GraphNode<>(new State(new Point(0, 0), Gear.TORCH));
        GraphNode<State> curr = root;

//        Stack<GraphNode<State>> stack = new Stack<>();
//        stack.push(root);

        Queue<GraphNode<State>> queue = new LinkedList<>();
        queue.add(root);

        while (!queue.isEmpty()) {
            curr = queue.remove();
            Cell cell = grid[curr.getValue().getY()][curr.getValue().getX()];

            //First check if we can move to the north
            Optional<Cell> northOpt = cell.getNorth();
            addStatesForAdjacentCell(graph, northOpt, queue, curr, cell);

            Optional<Cell> eastOpt = cell.getEast();
            addStatesForAdjacentCell(graph, eastOpt, queue, curr, cell);

            Optional<Cell> westOpt = cell.getWest();
            addStatesForAdjacentCell(graph, westOpt, queue, curr, cell);

            Optional<Cell> southOpt = cell.getSouth();
            addStatesForAdjacentCell(graph, southOpt, queue, curr, cell);
        }

        Graph.calculateOptimalPathsFromOrigin(graph, root);

        //Get the target node
        GraphNode<State> targetState = visited.get(new State(target, Gear.TORCH));

        printPath(targetState);

        return targetState.getDistance();
    }

    private void printPath(GraphNode<State> targetState) {
        State curr = null;
        for(GraphNode<State> node : targetState.getPath()) {
            State next = node.getValue();
            Cell cell = grid[next.getY()][next.getX()];
            if (curr == null) {
                System.out.println("Starting at " + next.getX() + "," + next.getY() + "[" + cell.getType() + "] with " + next.getGear() + "(" + node.getDistance() + ")");
                curr = next;
                continue;
            }

            System.out.println("Moving to " + next.getX() + "," + next.getY() + "[" + cell.getType() + "] with " + next.getGear() + "(" + node.getDistance() + ")");
            curr = next;
        }
    }

    private void addStatesForAdjacentCell(Graph<State> graph, Optional<Cell> next, Queue queue, GraphNode<State> curr, Cell currCell) {
        if (next.isPresent()) {
            Cell cell = next.get();
            List<Gear> compatibleGear = cell.getCompatibleGear(target);
            for (Gear g : compatibleGear) {

                //You can only change if the gear works in the place you currently are too!
                if (!currCell.getCompatibleGear(target).contains(g)) {
                    continue;
                }

                State nextState = new State(new Point(cell.getPt().x, cell.getPt().y), g);
                GraphNode<State> node = visited.get(nextState);
                if (node == null) {
                    node = new GraphNode<>(nextState);
                    visited.put(nextState, node);
                    graph.addNode(node);
                    queue.add(node);
                }

                //If it requires a change of gear then add 7 to the time taken
                int distance = 1;
                if (!g.equals(curr.getValue().getGear())) {
                    distance = distance + 7;
                }

                curr.addNeighbor(node, distance);
            }
        }
    }

    @Override
    protected String getName() {
        return "Mode Maze";
    }
}
