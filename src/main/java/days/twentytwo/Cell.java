package days.twentytwo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import sun.plugin.dom.exception.InvalidStateException;

import java.awt.*;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Getter
@AllArgsConstructor
public class Cell {

    public static int WIDTH = -1;
    public static Integer DEPTH = -1;
    private static final char ROCKY = '.';
    private static final char WET = '=';
    private static final char NARROW = '|';
    private static final char MOUTH = 'M';
    private static final char TARGET = 'T';
    public static final int X_MULTIPLIER = 16807;
    public static final int Y_MULTIPLIER = 48271;
    public static final int EROSION_MOD = 20183;
    public static final int TYPE_MOD = 3;

    private char type;
    private Point pt;
    private long erosionLevel;
    private long geoIndex;
    private Cell[][] grid;


    public static Cell of(int y, int x, Point target, int depth, Cell[][] grid) {
        long geoIndex = calcGeoIndex(y, x, target, grid);
        long erosionLevel = calcErosionLevel(geoIndex, depth);
        char type = calcType(erosionLevel);
        return new Cell(type, new Point(x,y), erosionLevel, geoIndex, grid);
    }

    private static char calcType(long erosionLevel) {
        long type = erosionLevel % TYPE_MOD;
        if (type == 0) {
            return ROCKY;
        }

        if (type == 1) {
            return WET;
        }

        if (type == 2) {
            return NARROW;
        }

        System.out.println("Unknown Type!");
        return 'X';
    }

    private static long calcErosionLevel(long geoIndex, int depth) {
        return (geoIndex + depth) % EROSION_MOD;
    }

    private static long calcGeoIndex(int y, int x, Point target, Cell[][] grid) {
        if (isMouth(y, x)) {
            return 0;
        }

        if (isTarget(y, x, target)) {
            return 0;
        }

        if (y==0) {
            return (long)x * X_MULTIPLIER;
        }

        if (x==0) {
            return (long)y * Y_MULTIPLIER;
        }

        return grid[y][x-1].erosionLevel * grid[y-1][x].erosionLevel;

    }

    private static boolean isTarget(int y, int x, Point target) {
        return y == target.y && x == target.x;
    }

    private static boolean isMouth(int y, int x) {
        return y==0 && x==0;
    }

    /**
     * Useful to have a reference back to the grid here
     * @param grid
     */
    public void setGrid(Cell[][] grid) {
        this.grid = grid;
    }

    @Override
    public String toString() {
        return "" + type;
    }

    private boolean boundCheck(int y, int x) {
        boolean maxBound = (y < DEPTH) && (x < WIDTH);
        boolean minBound = (y > -1) && (x > -1);
        return maxBound && minBound;
    }

    public boolean isRocky() {
        return type == ROCKY;
    }

    public boolean isWet() {
        return type == WET;
    }

    public boolean isNarrow() {
        return type == NARROW;
    }

    public Optional<Cell> getNorth() {
        if (!boundCheck(pt.y-1, pt.x)) {
            return Optional.empty();
        }
        return Optional.of(grid[pt.y-1][pt.x]);
    }

    public Optional<Cell> getSouth() {
        if (!boundCheck(pt.y+1, pt.x)) {
            return Optional.empty();
        }
        return Optional.of(grid[pt.y+1][pt.x]);
    }

    public Optional<Cell> getEast() {
        if (!boundCheck(pt.y, pt.x+1)) {
            return Optional.empty();
        }
        return Optional.of(grid[pt.y][pt.x+1]);
    }

    public Optional<Cell> getWest() {
        if (!boundCheck(pt.y, pt.x-1)) {
            return Optional.empty();
        }
        return Optional.of(grid[pt.y][pt.x-1]);
    }

    public int getRisk() {
        if (isRocky()) {
            return 0;
        }

        if (isWet()) {
            return 1;
        }

        if (isNarrow()) {
            return 2;
        }

        throw new InvalidStateException("No Risk found!");
    }

    public List<Gear> getCompatibleGear(Point target) {

        if (pt.getY() == target.getY() && pt.getX() == target.getX()) {
            System.out.println("Reached target - must have torch");
            return Arrays.asList(Gear.TORCH);
        }

        if (isRocky()) {
            return Arrays.asList(Gear.CLIMBING, Gear.TORCH);
        }

        if (isWet()) {
            return Arrays.asList(Gear.CLIMBING, Gear.NONE);
        }

        if (isNarrow()) {
            return Arrays.asList(Gear.TORCH, Gear.NONE);
        }

        throw new IllegalStateException("Should have found gear types");
    }
}
