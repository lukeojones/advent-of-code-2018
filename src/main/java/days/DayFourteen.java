package days;

import java.util.*;

public class DayFourteen extends DayOfAdvent<String, Integer> {

//    private static List<Long> TEST_1 = Arrays.asList(9L, 5158916779L);
//    private static List<Long> TEST_2 = Arrays.asList(5L, 124515891L);
//    private static List<Long> TEST_3 = Arrays.asList(18L, 9251071085L);
//    private static List<Long> TEST_4 = Arrays.asList(2018L, 5941429882L);
//    private static List<Long> REAL = Arrays.asList(260321L, null);
    private static Integer TEST_1 = 9;
    private static Integer TEST_2 = 5;
    private static Integer TEST_3 = 18;
    private static Integer TEST_4 = 2018;
    private static Integer REAL = 260321;

    private static String TEST_1b = "51589";
    private static String TEST_2b = "01245";
    private static String TEST_3b = "92510";
    private static String TEST_4b = "59414";
    private static String REAL_b = "260321";

    private static Integer SCENARIO = REAL;
    private static String SCENARIO_DIGITS = REAL_b;

    private static Integer BUFFER = 10;

    private ArrayDeque<Long> window = new ArrayDeque<>();
    private static List<Long> digits = getDigits(SCENARIO_DIGITS);
    private static int WINDOW_SIZE = digits.size();

    public DayFourteen(boolean testMode) {
        super("fourteen", testMode);
    }

    public static class Recipe {
        private Recipe next;
        private long value;

        public Recipe(long value) {
            this.value = value;
        }
    }

    public static class Elf {
        private Recipe currentRecipe;

        public Elf(Recipe currentRecipe) {
            this.currentRecipe = currentRecipe;
        }
    }

    @Override
    protected String solveFirst() {
//        Recipe recipeOne = new Recipe(3);
//        Recipe recipeTwo = new Recipe(7);
//        recipeOne.next = recipeTwo;
//        recipeTwo.next = recipeOne;
//
//        List<Recipe> recipes = new ArrayList<>();
//        recipes.add(recipeOne);
//        recipes.add(recipeTwo);
//
//        Elf elfOne = new Elf(recipeOne);
//        Elf elfTwo = new Elf(recipeTwo);
//
//        while (true) {
//            if (addRecipes(elfOne, elfTwo, recipes)){
//                break;
//            }
//
//            moveElf(elfOne, "one");
//            moveElf(elfTwo, "two");
//            //printRecipesDebug(recipes,elfOne, elfTwo);
//        }
//
//        return getAnswer(recipes, SCENARIO);
        return null;
    }

    private void printRecipesDebug(List<Recipe> recipes, Elf elfOne, Elf elfTwo) {
        int startIx = 0;
        for (int i = startIx; i<recipes.size(); i++) {
            Recipe recipe = recipes.get(i);
            String str = "" + recipe.value;
            if (recipe == elfOne.currentRecipe) {
                str = "(" + str + ")";
            } else if (recipe == elfTwo.currentRecipe) {
                str = "[" + str + "]";
            }
            System.out.print(str + " ");
        }
        System.out.println();
    }

    private String getAnswer(List<Recipe> recipes, int startIx) {
        StringBuilder sb = new StringBuilder();
        for (int i = startIx; i<startIx+BUFFER; i++) {
            Recipe recipe = recipes.get(i);
            sb.append(recipe.value);
        }
        return sb.toString();
    }

    private void moveElf(Elf elf, String name) {
        long originalRecipeValue = elf.currentRecipe.value;
//        System.out.println("Moving Elf " + name + " by " + (originalRecipeValue + 1) + " recipes");
        for (int i = 0; i<= originalRecipeValue; i++) {
            elf.currentRecipe = elf.currentRecipe.next;
        }
    }

    private Recipe getLastRecipe(List<Recipe> recipes) {
        return recipes.get(recipes.size()-1);
    }

    private boolean addRecipes(Elf elfOne, Elf elfTwo, List<Recipe> existingRecipes) {
        long total = elfOne.currentRecipe.value + elfTwo.currentRecipe.value;

        LinkedList<Long> digits = new LinkedList<>();
        if (total == 0) {
            digits.push(0L);
        } else {
            while (total > 0) {
                digits.push( total % 10 );
                total = total / 10;
            }
        }

        Recipe prev = getLastRecipe(existingRecipes);
        while (!digits.isEmpty()) {
            Long value = digits.pop();
            Recipe recipe = new Recipe(value);
            existingRecipes.add(recipe);

            if (matchWindow(recipe)) {
                //printRecipesDebug(existingRecipes, elfOne, elfTwo);
                return true;
            }

            prev.next = recipe; //add to the previously last recipe
            prev = recipe;

            //Do the length check here
            //Remove this for part 2
//            if (existingRecipes.size() >= (SCENARIO + BUFFER)) {
//                prev.next = existingRecipes.get(0);
//                return true;
//            }
        }

        //Link the last recipe to the beginning
        prev.next = existingRecipes.get(0);
        return false;
    }

    private boolean matchWindow(Recipe recipe) {
        boolean match = false;
        if (window.size() == WINDOW_SIZE) {
            window.removeFirst();
        }
        window.addLast(recipe.value);

        if (window.size() == WINDOW_SIZE) {
            match = true;
            Iterator<Long> iterator = window.iterator();
            int i = 0;
            //System.out.println("Window is: ");
            while (iterator.hasNext() && i < digits.size()) {
                Long next = iterator.next();
                //System.out.print(next);
                if (!next.equals(digits.get(i++))) {
                    match = false;
                    break;
                }
            }
            //System.out.println();
        }

        if (match) {
            System.out.println("Found match for window: " + SCENARIO_DIGITS);
        }
        return match;
    }

    private static List<Long> getDigits(String num) {
        List<Long> digits = new ArrayList<>();
        for (int i=0; i<num.length(); i++) {
            char c = num.charAt(i);
            long i1 = Long.parseLong("" + c);
            digits.add(i1);
        }

        return digits;
    }

    @Override
    protected Integer solveSecond() {
        Recipe recipeOne = new Recipe(3);
        Recipe recipeTwo = new Recipe(7);
        recipeOne.next = recipeTwo;
        recipeTwo.next = recipeOne;

        List<Recipe> recipes = new ArrayList<>();
        recipes.add(recipeOne);
        recipes.add(recipeTwo);

        matchWindow(recipeOne);
        matchWindow(recipeTwo);

        Elf elfOne = new Elf(recipeOne);
        Elf elfTwo = new Elf(recipeTwo);

        while (true) {
            if (addRecipes(elfOne, elfTwo, recipes)){
                break;
            }
            moveElf(elfOne, "one");
            moveElf(elfTwo, "two");
            //printRecipesDebug(recipes,elfOne, elfTwo);
        }

        return recipes.size() - digits.size(); //Arrrggghhhh! stupid magic number lost me hours
    }

    @Override
    protected String getName() {
        return "Chocolate Charts";
    }
}
