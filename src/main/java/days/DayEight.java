package days;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class DayEight extends DayOfAdvent<Integer, Integer> {

    private int metaTotal = 0;
    private Node root = null;

    public DayEight(boolean testMode) {
        super("eight", testMode);
    }

    @Override
    protected Integer solveFirst() {
        Scanner scanner = getScannerForInput();
        root = build(scanner);
        return this.metaTotal;
    }

    @Override
    protected Integer solveSecond() {
        return valueTraversal(root);
    }

    @Override
    protected String getName() {
        return "Memory Maneuver";
    }

    private static class Node {
        private int childCount = 0;
        private int metaDataCount = 0;
        private List<Integer> metaData = new ArrayList<>();
        private List<Node> children = new ArrayList<>();

        public Node(int childCount, int metaDataCount) {
            this.childCount = childCount;
            this.metaDataCount = metaDataCount;
        }

        public void addChild(Node child) {
            children.add(child);
            System.out.println("Adding " + child + " to node " + this);
        }

        public void addMetaData(Integer value) {
            metaData.add(value);
            System.out.println("Adding " + value + " to node " + this);
        }

        public int getMetaDataValueTotal() {
            int total = 0;
            for (int i=0; i<metaData.size(); i++) {
                total = total + metaData.get(i);
            }
            return total;
        }

        @Override
        public String toString() {
            return "[" + childCount + "," + metaDataCount + "]";
        }
    }

    public int valueTraversal(Node node) {
        if (node.childCount == 0) {
            return node.getMetaDataValueTotal();
        }

        int total = 0;
        for (int i=0; i<node.metaDataCount; i++) {
            int nodeIndex = node.metaData.get(i) - 1;
            if (nodeIndex < node.children.size() && nodeIndex > -1) {
                Node child = node.children.get(nodeIndex);
                total = total + valueTraversal(child);
            }
        }

        return total;
    }

    private Node build(Scanner scanner) {
        if (!scanner.hasNextInt()) {
            return null;
        }

        Node node = new Node(scanner.nextInt(), scanner.nextInt());
        for (int i=0; i<node.childCount; i++) {
            Node child = build(scanner);
            node.addChild(child);
        }

        for (int i=0; i<node.metaDataCount; i++) {
            Integer value = scanner.nextInt();
            node.addMetaData(value);
            metaTotal = metaTotal + value;
        }

        return node;
    }
}

//====================================================================================
//        Memory Maneuver solution (a)
//        Answer:	40746
//        Time:		500ms
//====================================================================================
//
//=====================================================================================
//        Memory Maneuver solution (b)
//        Answer:	37453
//        Time:		3ms
//=====================================================================================
