package days;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class DayOne {

    public static void main(String[] args) {
        DayOne solver = new DayOne();
//        solver.solveP1();
        int soln = solver.solveP2();
        System.out.println(soln);
    }

    private String getResourcePath(String resourceName) {
        ClassLoader classLoader = getClass().getClassLoader();
        return classLoader.getResource(resourceName).getFile();
    }

    public void solveP1() {
        try (Stream<String> stream = Files.lines(Paths.get(getResourcePath("input_one.txt")))) {
            int sum = stream.mapToInt(Integer::parseInt)
                    .sum();
            System.out.println(sum);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public int solveP2() {
        HashSet<Integer> frequencies = new HashSet<>();
        frequencies.add(0);

        try (Stream<String> stream = Files.lines(Paths.get(getResourcePath("input_one.txt")))) {
            List<Integer> changes = stream.mapToInt(Integer::parseInt)
                    .boxed()
                    .collect(Collectors.toList());

            int frequency = 0;
            int frequencyChanges = changes.size();
            for (int i = 0; i < frequencyChanges; i++) {
                frequency += changes.get(i);
                if (frequencies.contains(frequency)) {
                    return frequency;
                }
                frequencies.add(frequency);

                //Reset the list if we haven't found a duplicate yet
                if (i == frequencyChanges-1) {
                    System.out.println("Looping...");
                    i = -1;
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return -1;
    }
}
