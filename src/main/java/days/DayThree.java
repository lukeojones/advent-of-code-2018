package days;

import java.util.List;

public class DayThree extends DayOfAdvent<Integer, Integer> {

    //private AdventParser parser = AdventParser.ofRegex(#1 @ 146,196: 19x14)
    private int[][] fabric = new int[1000][1000];

    public DayThree(boolean testMode) {
        super("three", testMode);
    }

    @Override
    protected Integer solveFirst() {
        List<String> lines = getFileLinesList();
        for (String claim : lines) {
            //#1 @ 1,3: 4x4
            int ix_at = claim.indexOf("@");
            int ix_colon = claim.indexOf(":");
            String co_ords = claim.substring(ix_at+2, ix_colon);
            String dims = claim.substring(ix_colon+2);

            String[] co_ord_toks = co_ords.split(",");
            String[] dim_toks = dims.split("x");

            int x = Integer.parseInt(co_ord_toks[0]);
            int y = Integer.parseInt(co_ord_toks[1]);

            int w = Integer.parseInt(dim_toks[0]);
            int h = Integer.parseInt(dim_toks[1]);

            for (int i=x; i<x+w; i++) {
                for (int j=y; j<y+h; j++) {
                    fabric[i][j] = fabric[i][j] + 1;
                }
            }
        }

        int count = 0;
        for (int i=0; i < 1000; i++) {
            for (int j=0; j < 1000; j++) {
                if (fabric[i][j] > 1) {
                    count ++;
                }
            }
        }

        return count;
    }

    @Override
    protected Integer solveSecond() {
        List<String> lines = getFileLinesList();
        for (String claim : lines) {
            //#1 @ 1,3: 4x4
            int ix_claim_id = claim.indexOf("#");
            int ix_at = claim.indexOf("@");
            int ix_colon = claim.indexOf(":");
            String claim_id = claim.substring(ix_claim_id+1, ix_at-1);
            String co_ords = claim.substring(ix_at+2, ix_colon);
            String dims = claim.substring(ix_colon+2);

            String[] co_ord_toks = co_ords.split(",");
            String[] dim_toks = dims.split("x");

            int x = Integer.parseInt(co_ord_toks[0]);
            int y = Integer.parseInt(co_ord_toks[1]);

            int w = Integer.parseInt(dim_toks[0]);
            int h = Integer.parseInt(dim_toks[1]);

            boolean unused = true;
            for (int i=x; i<x+w; i++) {
                for (int j=y; j<y+h; j++) {
                    if (fabric[i][j] > 1) {
                        unused = false;
                        break;
                    }
                }
            }

            if (unused) {
                return Integer.parseInt(claim_id);
            }
        }
        return -1;
    }

    @Override
    protected String getName() {
        return "No Matter How You Slice It";
    }
}
