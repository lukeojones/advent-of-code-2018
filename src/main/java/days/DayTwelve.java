package days;

import utils.AdventParser;

import java.util.BitSet;
import java.util.HashMap;
import java.util.List;

public class DayTwelve extends DayOfAdvent<Integer, Long> {

    private static int GENERATIONS = 1000;
    private static int PADDING = 1000;
    private static Character PLANT = '#';
    private static Character NO_PLANT = '.';
    private AdventParser generatorParser = AdventParser.ofRegex("(.*) => (.*)");
    private AdventParser initialStateParser = AdventParser.ofRegex("initial state: (.*)");

    private BitSet state;
    private HashMap<BitSet, Generator> generators = new HashMap<>();
    private int nInitialPots;


    public DayTwelve(boolean testMode) {
        super("twelve", testMode);
    }

    public static class Generator {
        private BitSet mask;
        private boolean result;

        public Generator(BitSet mask, boolean result) {
            this.mask = mask;
            this.result = result;
        }
    }

    private Generator getGenerator(String str) {
        BitSet mask = new BitSet(5);
        String[] strs = generatorParser.getTokens(str, true);
        for (int i=0; i<strs[0].length(); i++) {
            char c = strs[0].charAt(i);
            if (c == PLANT) {
                mask.set(i);
            }
        }
        return new Generator(mask, strs[1].charAt(0) == PLANT);
    }

    private BitSet getInitialState(String str) {
        String[] tokens = initialStateParser.getTokens(str, true);
        nInitialPots = tokens[0].length();
        int nRightPots = (nInitialPots-1) + PADDING;
        int nLeftPots = (nInitialPots -1) + PADDING;
        int nTotalPots = nLeftPots + 1 + nRightPots; //left + centre + right
        BitSet state = new BitSet(nTotalPots);
        for (int i = nLeftPots; i < (nLeftPots+nInitialPots); i++) {
            char c = tokens[0].charAt(i-nLeftPots);
            if (c == '#') {
                state.set(i, true);
            }
        }
        return state;
    }

    @Override
    protected Integer solveFirst() {
        List<String> lines = getFileLinesList();

        //Setup the initial state
        state = getInitialState(lines.get(0));
        //System.out.println("State I:\t\t [" + printBitSet(state) + "]");

        //Get all the generators
        for (int i=2; i<lines.size(); i++) {
            String line = lines.get(i);
            Generator generator = getGenerator(line);
            generators.put(generator.mask, generator);
        }

        int prev = 0;
        for (int i=1;i<=GENERATIONS;i++) {
            applyGenerators();
            //System.out.println("State:\t\t\t [" + printBitSet(state) + "]");
            int conut = countPlantPotNumbers();
            System.out.println("Gen: " + i + " : " + conut + " - Diff: " + (conut - prev));
            prev = conut;
        }

        return countPlantPotNumbers();
    }

    private String printBitSet(BitSet set) {
        StringBuilder sb = new StringBuilder();
        for (int i=0; i<set.size(); i++) {
            if (set.get(i)) {
                sb.append(PLANT);
            } else {
                sb.append(NO_PLANT);
            }
        }
        return sb.toString();
    }

    private void applyGenerators() {
        //System.out.println("Generator:\t\t [" + printBitSet(generator.mask) + "]");
        int size = state.size();
        //BitSet newState = (BitSet)state.clone();
        BitSet newState = new BitSet(state.size());
        for (int pot = 2; pot<(size-2); pot++) {
            BitSet potset = state.get(pot - 2, pot + 2 + 1);
            Generator generator = generators.get(potset);
            if (generator != null) {
                //System.out.println("Match Potset: [" + printBitSet(potset) + "] to generator [" + printBitSet(generator.mask));
                newState.set(pot, generator.result);
            }
        }
        state = newState;
    }

    private int countPlantPotNumbers() {
        int count = 0;
        for (int i=0; i<state.size(); i++) {
            if (state.get(i)) {
                //System.out.println("Plant at: " + (i-(PADDING+nInitialPots)+1));
                count = count + (i-(PADDING+nInitialPots)+1);
            }
        }
        return count;
    }

    @Override
    protected Long solveSecond() {
        long generations = 50_000_000_000L;
        return ((generations - 732) * 34) + 24899;
    }

    @Override
    protected String getName() {
        return "Subterranean Sustainability";
    }
}
