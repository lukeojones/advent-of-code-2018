package days.twentyfour;

import days.DayOfAdvent;
import utils.AdventParser;

import java.util.*;
import java.util.stream.Collectors;

public class DayTwentyFour extends DayOfAdvent<Integer, Integer> {

    public static int BOOST = 50;
    private List<Group> originalGroups = new ArrayList<>();
    private static final AdventParser parser = AdventParser.ofRegex("(.*) units each with (.*) hit points with an attack that does (.*) (.*) damage at initiative (.*)");

    public DayTwentyFour(boolean testMode) {
        super("twentyfour", testMode);
    }

    @Override
    protected Integer solveFirst() {
        List<Group> groups = parseSquadrons();
        groups = parseSquadrons();

        //System.out.println("Squadrons size: " + groups.size());

        while (haveActiveInfectionGroups(groups) && haveActiveImmuneGroups(groups)) {
            Set<Group> victims = new HashSet<>();
            for (Group ag : groups) {
                ag.setTarget(null);
                List<Group> enemyGroups = ag.getEnemyGroups(groups, victims);

                if (enemyGroups.isEmpty()) {
                    continue;
                }
                Group victim = enemyGroups.get(0);
                if (victim.getImpact(ag) == 0) {
                    continue; //don't attack if there isn't any damage to be dealt
                }

                ag.setTarget(victim);
                victims.add(victim);
            }

            Collections.sort(groups, new Group.InitiativeComparator());
            for (Group ag : groups) {
                ag.attackTarget();
            }
            //System.out.println();

            groups = getActiveGroups(groups);
        }

        List<Group> active;
        if (haveActiveInfectionGroups(groups)) {
            active = getActiveGroupsOfType(groups, Group.Type.INFECTION);
        } else {
            active = getActiveGroupsOfType(groups, Group.Type.IMMUNE_SYSTEM);
        }

        return active.stream()
                .mapToInt(Group :: getUnits)
                .sum();
    }

    private boolean haveActiveInfectionGroups(List<Group> groups) {
        return getActiveGroupsOfType(groups, Group.Type.INFECTION).size() > 0;
    }

    private boolean haveActiveImmuneGroups(List<Group> groups) {
        return getActiveGroupsOfType(groups, Group.Type.IMMUNE_SYSTEM).size() > 0;
    }

    private List<Group> getActiveGroupsOfType(List<Group> groups, Group.Type type) {
        return groups.stream()
                .filter(g -> g.getType() == type)
                .filter(Group :: isActive)
                .collect(Collectors.toList());
    }

    private List<Group> getActiveGroups(List<Group> groups) {
        return groups.stream()
                .filter(Group :: isActive)
                .sorted(new Group.AttackGroupComparator())
                .collect(Collectors.toList());
    }

    private List<Group> parseSquadrons() {
        if (!originalGroups.isEmpty()) {
            return originalGroups.stream()
                    .map(Group::new)
                    .collect(Collectors.toList());
        }

        List<Group> groups = new ArrayList<>();
        List<String> lines = getFileLinesList();
        Group.Type type = Group.Type.IMMUNE_SYSTEM;

        for (String line : lines) {
            if (line.startsWith("Immune System:")) {
                type = Group.Type.IMMUNE_SYSTEM;
                continue;
            }

            if (line.startsWith("Infection:")) {
                type = Group.Type.INFECTION;
                continue;
            }

            if (line.isEmpty()) {
                continue;
            }

            groups.add(Group.of(line, type));
            originalGroups.add(Group.of(line, type));
        }

        originalGroups = getActiveGroups(originalGroups);
        return getActiveGroups(groups);
    }

    @Override
    protected Integer solveSecond() {
        List<Group> active = new ArrayList<>();
        while (active.isEmpty()) {
            BOOST++;
            List<Group> groups = parseSquadrons();
            groups = getActiveGroups(originalGroups);
            while (haveActiveInfectionGroups(groups) && haveActiveImmuneGroups(groups)) {
                Set<Group> victims = new HashSet<>();
                for (Group ag : groups) {
                    ag.setTarget(null);
                    List<Group> enemyGroups = ag.getEnemyGroups(groups, victims);

                    if (enemyGroups.isEmpty()) {
                        continue;
                    }
                    Group victim = enemyGroups.get(0);
                    if (victim.getImpact(ag) == 0) {
                        continue; //don't attack if there isn't any damage to be dealt
                    }

                    ag.setTarget(victim);
                    victims.add(victim);
                }

                Collections.sort(groups, new Group.InitiativeComparator());
                boolean deadlock = true;
                for (Group ag : groups) {
                    if (ag.getTarget() != null) {
                        deadlock = false;
                    }
                    ag.attackTarget();
                }
                //System.out.println();

                if (deadlock) {
                    break;
                }

                groups = getActiveGroups(groups);
            }

            if (haveActiveInfectionGroups(groups)) {
                System.out.println("Infection wins (" + BOOST + ")");
            } else {
                active = getActiveGroupsOfType(groups, Group.Type.IMMUNE_SYSTEM);
            }
            break;
        }

        return active.stream()
                .mapToInt(Group :: getUnits)
                .sum();
    }

    @Override
    protected String getName() {
        return "Immune System Simulator 20XX";
    }
}
