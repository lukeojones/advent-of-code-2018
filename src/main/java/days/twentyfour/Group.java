package days.twentyfour;

import lombok.Getter;
import utils.AdventParser;

import java.util.*;

@Getter
public class Group {
    private static final AdventParser iwparser = AdventParser.ofRegex("(.*) units each with (.*) hit points \\((.*)\\) with an attack that does (.*) (.*) damage at initiative (.*)");
    private static final AdventParser parser = AdventParser.ofRegex("(.*) units each with (.*) hit points with an attack that does (.*) (.*) damage at initiative (.*)");
    private static final String WEAKNESS = "weak to";
    private static final String IMMUNITY = "immune to";

    private static Map<Type, Integer> typeIds = new HashMap<>();

    private final String id;
    private final Type type;
    private int units;
    private final int hp;
    private final Attack attack;
    private final int initiative;
    private final List<Power> immunities;
    private final List<Power> weaknesses;
    private Group target;

    private Group(String id, Type type, int units, int hp, Attack attack, int initiative, List<Power> immunities, List<Power> weaknesses) {
        this.id = id;
        this.type = type;
        this.units = units;
        this.hp = hp;
        this.attack = attack;
        this.initiative = initiative;
        this.immunities = immunities;
        this.weaknesses = weaknesses;
    }

    public Group(Group group) {
        this(group.id, group.type, group.units, group.hp, new Attack(group.attack), group.initiative, new ArrayList(group.immunities), new ArrayList(group.weaknesses));
    }

    public static Group of(String line, Type type) {
        Group group = null;

        int typeId = typeIds.getOrDefault(type, Integer.valueOf(1));
        typeIds.put(type, typeId + 1);
        String id = "" + type.toString() + typeId;

        if (iwparser.matches(line)) {
            String[] toks = iwparser.getTokens(line, true);
            group = new Group(id,
                    type,
                    Integer.valueOf(toks[0]),
                    Integer.valueOf(toks[1]),
                    Attack.of(Integer.valueOf(toks[3]), toks[4]),
                    Integer.valueOf(toks[5]),
                    parseImmunities(toks[2]),
                    parseWeaknesses(toks[2]));
        } else if (parser.matches(line)) {
            String[] toks = parser.getTokens(line, true);
            group = new Group(id,
                    type,
                    Integer.valueOf(toks[0]),
                    Integer.valueOf(toks[1]),
                    Attack.of(Integer.valueOf(toks[2]), toks[3]),
                    Integer.valueOf(toks[4]),
                    Collections.emptyList(),
                    Collections.emptyList());
        }
        return group;
    }

    public static List<Power> parseWeaknesses(String str) {
        return parsePowers(WEAKNESS, str);
    }

    public static List<Power> parseImmunities(String str) {
        return parsePowers(IMMUNITY, str);
    }

    private static List<Power> parsePowers(String type, String str) {
        List<Power> powers = new ArrayList<>();
        String[] toks = str.split(";");

        //Assume it's in first section
        String toParse = toks[0];
        int ix = toParse.indexOf(type);

        //If not in first section look in second
        if (ix == -1 && (toks.length > 1)) {
            toParse = toks[1];
            ix = toParse.indexOf(type);
        }
        if (ix > -1) {
            String list = toParse.substring(ix + type.length() + 1);
            String[] wks = list.split(",");
            for (String wk: wks) {
                Power power = Power.valueOf(wk.trim().toUpperCase());
                powers.add(power);
            }
        }
        return powers;
    }

    public int getEffectivePower() {
        int boost = type == Type.IMMUNE_SYSTEM ? DayTwentyFour.BOOST : 0;
        return units * (attack.ap + boost);
    }

    public Type getEnemyType() {
        return type == Type.IMMUNE_SYSTEM ? Type.INFECTION : Type.IMMUNE_SYSTEM;
    }

    public boolean isActive() {
        return units > 0;
    }

    public List<Group> getEnemyGroups(List<Group> groups, Set<Group> victims) {
        List<Group> enemyGroups = new ArrayList<>();
        for (Group grp : groups) {
            if (grp.getType() == getEnemyType() && !victims.contains(grp)) {
                enemyGroups.add(grp);
            }
        }
        Collections.sort(enemyGroups, new VictimGroupComparator(this));
        return enemyGroups;
    }

    public void setTarget(Group target) {
        this.target = target;
    }

    public enum Power {
        RADIATION, FIRE, COLD, SLASHING, BLUDGEONING
    }

    public enum Type {
        IMMUNE_SYSTEM, INFECTION;
    }

    public static class VictimGroupComparator implements Comparator<Group> {

        private final Group attackingGroup;

        public VictimGroupComparator(Group attackingGroup) {
            this.attackingGroup = attackingGroup;
        }

        @Override
        public int compare(Group first, Group second) {
            int dImpact = second.getImpact(attackingGroup) - first.getImpact(attackingGroup);
            if (dImpact == 0) {
                int dEffectivePower = second.getEffectivePower() - first.getEffectivePower();
                if (dEffectivePower == 0) {
                    return second.getInitiative() - first.getInitiative();
                }
                return dEffectivePower;
            }
            return dImpact;
        }
    }

    /**
     * Return the impact that a given attack would have on this unit.
     * @return
     */
    public int getImpact(Group attackingGroup) {

        if (immunities.contains(attackingGroup.attack.power)) {
            return 0;
        }

        if (weaknesses.contains(attackingGroup.attack.power)) {
            return 2 * attackingGroup.getEffectivePower();
        }

        return attackingGroup.getEffectivePower();
    }

    /**
     * Suffer the given attack
     * Return true if unit is still alive
     */
    public void attacked(Group attackingGroup) {
        int impact = getImpact(attackingGroup);
        units = Math.max(units - (impact / hp), 0);
    }

    public void attackTarget() {
        if (target == null) {
            return;
        }

        if (units < 1) {
            return; //Can't attack with no units
        }

        //System.out.println(this.id + " is attacking " + target.getId());

        target.attacked(this);
    }

    public static class InitiativeComparator implements Comparator<Group> {
        @Override
        public int compare(Group first, Group second) {
            return second.getInitiative() - first.getInitiative();
        }
    }

    public static class AttackGroupComparator implements Comparator<Group> {
        @Override
        public int compare(Group first, Group second) {
            int efp = second.getEffectivePower() - first.getEffectivePower();
            if (efp == 0) {
                return second.getInitiative() - first.getInitiative();
            }
            return efp;
        }
    }

    public static class Attack {
        private int ap;
        private Power power;

        private Attack(int ap, Power power) {
            this.ap = ap;
            this.power = power;
        }

        public Attack(Attack attack) {
            this(attack.ap, attack.power);
        }

        public static Attack of(int ap, String power) {
            return new Attack(ap, Power.valueOf(power.toUpperCase()));
        }
    }
}
