package days;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
//todo - Horrid! eeds massive cleanup and refactoring
public class DayFour extends DayOfAdvent<Integer, Integer> {


    public DayFour(boolean testMode) {
        super("four", testMode);
    }

    @Override
    protected Integer solveFirst() {
        List<GuardEvent> events = new ArrayList<>();
        List<String> lines = getFileLinesList();
        for (String line : lines) {
            events.add(new GuardEvent(line));
        }

        Collections.sort(events);

        HashMap<Integer, Integer> idToCount = new HashMap<>();
        int i=0;
        while (i < events.size()-2) {
            GuardEvent shiftEvent = events.get(i++);

            while (i < events.size()-1 && events.get(i).eventType != 1) {
                GuardEvent sleepEvent = events.get(i++);
                GuardEvent wakeEvent = events.get(i++);

                int guardId = shiftEvent.guardId;
                if (guardId == -1) {
                    System.out.println("Found Bad ID!");
                }

                sleepEvent.guardId = guardId;
                wakeEvent.guardId = guardId;

                int sleepMins = sleepEvent.dtStart.getMinute();
                int wakeMins = wakeEvent.dtStart.getMinute();
                int diffMins = wakeMins - sleepMins;

                Integer totalSleep = idToCount.get(guardId);
                if (totalSleep == null) {
                    idToCount.put(guardId, diffMins);
                } else {
                    idToCount.put(guardId, diffMins + totalSleep);
                }
            }
        }

        Iterator<Integer> iterator = idToCount.keySet().iterator();
        int sleepiestGuard = -1;
        int maxCount = -1;
        while (iterator.hasNext()) {
            int guardId = iterator.next();
            int totalCount = idToCount.get(guardId);
            if (totalCount > maxCount) {
                maxCount = totalCount;
                sleepiestGuard = guardId;
            }
        }

        //System.out.println("Sleepy Guard = " + sleepiestGuard);

        //Get all the events for this guard
        List<GuardEvent> eventsForSleepyGuard = new ArrayList<>();
        for (int j=0; j<events.size(); j++) {
            GuardEvent event = events.get(j);
            if (event.guardId == sleepiestGuard) {
                eventsForSleepyGuard.add(event);
            }
        }

        //Now do frequency map of each minute asleep for this guard
        HashMap<Integer, Integer> minToTimesAsleep = new HashMap<>(60);

        int k=0;
        while (k < eventsForSleepyGuard.size()-2) {
            GuardEvent shiftEvent = eventsForSleepyGuard.get(k++);

            while (k < eventsForSleepyGuard.size()-1 && eventsForSleepyGuard.get(k).eventType != 1) {
                GuardEvent sleepEvent = eventsForSleepyGuard.get(k++);
                GuardEvent wakeEvent = eventsForSleepyGuard.get(k++);

                for (int min=0; min<60; min++) {
                    if (min >= sleepEvent.dtStart.getMinute() && min < wakeEvent.dtStart.getMinute()) {
                        Integer timesAsleep = minToTimesAsleep.get(min);
                        if (timesAsleep == null) {
                            minToTimesAsleep.put(min, 1);
                        } else {
                            minToTimesAsleep.put(min, timesAsleep + 1);
                        }
                    }
                }
            }
        }

        Iterator<Integer> iteratorMins = minToTimesAsleep.keySet().iterator();
        int sleepiestMin = -1;
        int maxMinuteCount = -1;
        while (iteratorMins.hasNext()) {
            int minute = iteratorMins.next();
            int totalCount = minToTimesAsleep.get(minute);
            if (totalCount > maxMinuteCount) {
                maxMinuteCount = totalCount;
                sleepiestMin = minute;
            }
        }

        return sleepiestGuard * sleepiestMin;
    }

    @Override
    protected Integer solveSecond() {
        List<GuardEvent> events = new ArrayList<>();
        List<String> lines = getFileLinesList();
        for (String line : lines) {
            events.add(new GuardEvent(line));
        }

        Collections.sort(events);

        int maxGuard = -1;
        int maxFrequency = -1;
        int maxMin = -1;

        HashMap<Integer, List<Integer>> minToGuardsAsleep = new HashMap<>(60);
        int i = 0;
        while (i < events.size() - 2) {
            GuardEvent shiftEvent = events.get(i++);

            while (i < events.size() - 1 && events.get(i).eventType != 1) {
                GuardEvent sleepEvent = events.get(i++);
                GuardEvent wakeEvent = events.get(i++);

                int guardId = shiftEvent.guardId;
                if (guardId == -1) {
                    System.out.println("Found Bad ID!");
                }

                sleepEvent.guardId = guardId;
                wakeEvent.guardId = guardId;

                int sleepMins = sleepEvent.dtStart.getMinute();
                int wakeMins = wakeEvent.dtStart.getMinute();

                for (int min = 0; min < 60; min++) {
                    if (min >= sleepMins && min < wakeMins) {
                        List<Integer> guardsAsleep = minToGuardsAsleep.get(min);
                        if (guardsAsleep == null) {
                            ArrayList<Integer> guards = new ArrayList<>();
                            guards.add(guardId);
                            minToGuardsAsleep.put(min, guards);
                        } else {
                            guardsAsleep.add(guardId);
                        }
                    }
                }

                //Now get the most common guard in each list
                Iterator<Integer> mins = minToGuardsAsleep.keySet().iterator();
                while (mins.hasNext()) {
                    Integer min = mins.next();
                    List<Integer> guards = minToGuardsAsleep.get(min);

                    HashMap<Integer, Integer> guardFrequency = new HashMap<>();
                    for (Integer guard : guards) {
                        Integer frequency = guardFrequency.get(guard);
                        if (frequency == null) {
                            frequency = 1;
                            guardFrequency.put(guard, frequency);
                        } else {
                            frequency++;
                            guardFrequency.put(guard, frequency);
                        }

                        if (frequency > maxFrequency) {
                            maxFrequency = frequency;
                            maxGuard = guard;
                            maxMin = min;
                        }
                    }
                }
            }
        }

        return maxGuard * maxMin;
    }

    @Override
    protected String getName() {
        return "Repose Record";
    }

    public static class GuardEvent implements Comparable<GuardEvent> {
        private static DateTimeFormatter f = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");

        private LocalDateTime dtStart;
        private int guardId = -1;
        private int eventType = -1; //1 = Begin Shift, 2 = Fall Asleep, 3 = Wake-up


        /**
         [1518-11-01 00:00] Guard #10 begins shift
         [1518-11-01 00:05] falls asleep
         [1518-11-01 00:25] wakes up
         **/
        public GuardEvent(String line) {
            int ixClose = line.indexOf(']');
            String dtString = line.substring(1, ixClose);
            this.dtStart = LocalDateTime.from(f.parse(dtString));

            if (line.indexOf("begins shift") > -1) {
                this.eventType = 1;
                this.guardId = parseGuardId(line);
            } else if (line.indexOf("falls asleep") > -1) {
                this.eventType = 2;
            } else if (line.indexOf("wakes up") > -1) {
                this.eventType = 3;
            }
        }

        private int parseGuardId(String line) {
            int ixHash = line.indexOf('#');
            int ixBegin = line.indexOf("begins");
            String guardId = line.substring(ixHash+1, ixBegin-1);
            return Integer.parseInt(guardId);
        }

        @Override
        public int compareTo(GuardEvent o) {
            return this.dtStart.compareTo(o.dtStart);
        }
    }
}

//====================================================================================
//        Repose Record solution (a)
//        Answer:	77084
//        Time:		193ms
//====================================================================================
//
//=====================================================================================
//        Repose Record solution (b)
//        Answer:	23047
//        Time:		194ms
//=====================================================================================