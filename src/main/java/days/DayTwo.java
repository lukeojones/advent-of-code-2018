package days;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class DayTwo {

    public static void main(String[] args) {
        DayTwo solver = new DayTwo();
        long start = System.currentTimeMillis();
        solver.solveP2();
        long finish = System.currentTimeMillis();

        long startOpt = System.currentTimeMillis();
        System.out.println(solver.solveP2_opt());
        long finishOpt = System.currentTimeMillis();

        System.out.println("Normal: " + (finish - start) + "ms");
        System.out.println("Normal Opt: " + (finishOpt - startOpt) + "ms");
    }

    private String getResourcePath(String resourceName) {
        ClassLoader classLoader = getClass().getClassLoader();
        return classLoader.getResource(resourceName).getFile();
    }

    public void solveP1() {
        try (Stream<String> stream = Files.lines(Paths.get(getResourcePath("input_two.txt")))) {
            List<String> ids = stream.collect(Collectors.toList());

            int doubles = 0;
            int triples = 0;
            for (String id : ids) {
                HashMap<Character, Integer> frequency = getFrequency(id);
                Set<Character> characters = frequency.keySet();

                boolean hasTriple = false;
                boolean hasDouble = false;

                Iterator<Character> itr = characters.iterator();
                while (itr.hasNext()) {
                    Character c = itr.next();
                    if (frequency.get(c) == 2) {
                        hasDouble = true;
                    } else if (frequency.get(c) == 3) {
                        hasTriple = true;
                    }
                }

                if (hasDouble) {
                    doubles++;
                }

                if (hasTriple) {
                    triples++;
                }
            }

            System.out.println(doubles * triples);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private HashMap<Character, Integer> getFrequency(String id) {
        HashMap<Character, Integer> counts = new HashMap<>();
        for (int i = 0; i < id.length(); i++) {
            Character c = id.charAt(i);

            Integer existingCount = counts.get(c);
            if (existingCount != null) {
                counts.put(c, existingCount + 1);
            } else {
                counts.put(c, 1);
            }
        }

        return counts;
    }

    public void solveP2() {
        try (Stream<String> stream = Files.lines(Paths.get(getResourcePath("input_two.txt")))) {
            List<String> ids = stream.collect(Collectors.toList());

            for (int i=0; i<ids.size(); i++) {
                String id = ids.get(i);
                String matchingId = findMatch(i, id, ids);
                if (matchingId != null) {
                    System.out.println("String 1: " + id);
                    System.out.println("String 2: " + matchingId);
                    return;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String findMatch(int index, String id, List<String> ids) {
        for (int i=index+1; i<ids.size(); i++) {
            String otherId = ids.get(i);

            int diffChars = 0;
            for (int j=0; j<otherId.length(); j++) {
                if (id.charAt(j) != otherId.charAt(j)) {
                    diffChars++;
                }
            }

            if (diffChars == 1) {
                return otherId;
            }
        }

        return null;
    }

    public String solveP2_opt() {
        File src = new File(getResourcePath("input_two_test_b.txt"));

        HashMap<String, String> shortToFull = new HashMap<String, String>();
        try (BufferedReader br = new BufferedReader(new FileReader(src))) {
            String id;
            while ((id = br.readLine()) != null) {
                for (int i=0; i<id.length(); i++) {
                    String dummyId = createDummyId(id, i);
                    String match = shortToFull.get(dummyId);
                    if (match != null) {
                        return dummyId.replace("?", "");
                    } else {
                        shortToFull.put(dummyId, id);
                    }
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return "Not Found";
    }

    private String createDummyId(String id, int i) {
        return id.substring(0,i) + '?' + id.substring(i+1);
    }

}