package days.sixteen;

public class EqriOperation extends AbstractOperation {

    public EqriOperation(DaySixteen.Register rA, DaySixteen.Register rB) {
        super("eqri", rA, rB);
    }

    @Override
    public int apply() {
        return rA.getContent() == rB.getValue() ? 1 : 0;
    }
}
