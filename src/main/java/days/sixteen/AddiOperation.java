package days.sixteen;

public class AddiOperation extends AbstractOperation {

    public AddiOperation(DaySixteen.Register rA, DaySixteen.Register rB) {
        super("addi", rA, rB);
    }

    @Override
    public int apply() {
        return rA.getContent() + rB.getValue();
    }
}
