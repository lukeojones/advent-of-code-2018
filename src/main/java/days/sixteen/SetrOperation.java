package days.sixteen;

public class SetrOperation extends AbstractOperation {

    public SetrOperation(DaySixteen.Register rA, DaySixteen.Register rB) {
        super("setr", rA, rB);
    }

    @Override
    public int apply() {
        return rA.getContent();
    }
}
