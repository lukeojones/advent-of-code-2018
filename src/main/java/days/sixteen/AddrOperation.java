package days.sixteen;

public class AddrOperation extends AbstractOperation {

    public AddrOperation(DaySixteen.Register rA, DaySixteen.Register rB) {
        super("addr", rA, rB);
    }

    @Override
    public int apply() {
        return rA.getContent() + rB.getContent();
    }
}
