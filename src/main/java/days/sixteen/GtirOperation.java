package days.sixteen;

public class GtirOperation extends AbstractOperation {

    public GtirOperation(DaySixteen.Register rA, DaySixteen.Register rB) {
        super("gtir", rA, rB);
    }

    @Override
    public int apply() {
        return rA.getValue() > rB.getContent() ? 1 : 0;
    }
}
