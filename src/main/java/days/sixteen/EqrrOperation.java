package days.sixteen;

public class EqrrOperation extends AbstractOperation {

    public EqrrOperation(DaySixteen.Register rA, DaySixteen.Register rB) {
        super("eqrr", rA, rB);
    }

    @Override
    public int apply() {
        return rA.getContent() == rB.getContent() ? 1 : 0;
    }
}
