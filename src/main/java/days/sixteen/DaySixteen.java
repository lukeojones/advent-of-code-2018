package days.sixteen;

import days.DayOfAdvent;
import javafx.util.Pair;
import lombok.Getter;
import lombok.Setter;
import utils.AdventParser;

import java.util.*;

public class DaySixteen extends DayOfAdvent<Integer,Integer> {

    public DaySixteen(boolean testMode) {
        super("sixteen", testMode);
    }
    private static HashMap<Integer, Set<String>> opIdToCommand = new HashMap<>();
    private static HashMap<Integer, Set<String>> opIdToNotCommand = new HashMap<>();

    private static HashMap<String, Set<Integer>> opNameToPossibleOpNum = new HashMap<>();
    private static HashMap<String, Set<Integer>> opNameToImpossibleOpNum = new HashMap<>();

    private static HashMap<Integer, String> hmKnownCodes = new HashMap<>();

    static int nTripleSamples = 0;

    @Override
    protected Integer solveFirst() {
        List<Sample> samples = parse().getKey();
        for (int i=0; i<samples.size(); i++) {
            Sample s = samples.get(i);
            s.processOperations();
        }
        return nTripleSamples;
    }

    private Pair<List<Sample>, List<Instruction>> parse() {
        List<String> lines = getFileLinesList();

        List<Sample> samples = new ArrayList<>();

        int nextSectionLineNumber = 0;
        for (int l=0; l<=lines.size()-Sample.SAMPLE_LINES;l++) {
            if (lines.get(l).startsWith("*****")) {
                nextSectionLineNumber = l+1;
                break;
            }

            String input = lines.get(l++);
            String instruction = lines.get(l++);
            String output = lines.get(l++);
            samples.add(Sample.of(input, output, instruction));
        }

        List<Instruction> instructions = getInstructions(lines, nextSectionLineNumber);


        System.out.println("Processed " + samples.size() + " samples");
        return new Pair<>(samples, instructions);
    }

    private List<Instruction> getInstructions(List<String> lines, int nextSectionLineNumber) {
        List<Instruction> instructions = new ArrayList<>();
        if (nextSectionLineNumber > 0) {
            for (int l=nextSectionLineNumber; l<lines.size();l++) {
                String instruction = lines.get(l);
                instructions.add(Instruction.of(instruction));
            }
        }
        return instructions;
    }

    @Override
    protected Integer solveSecond() {
        Pair<List<Sample>, List<Instruction>> parseResults = parse();
        List<Sample> samples = parseResults.getKey();
        for (int i=0; i<samples.size(); i++) {
            Sample s = samples.get(i);
            s.processOperations();
        }

        resolve();
        calculateCodes();

        List<Instruction> instructions = parseResults.getValue();
        return runProgram(instructions);
    }

    private Integer runProgram(List<Instruction> instructions) {
        Register reg0 = new Register(0, 0);
        Register reg1 = new Register(1, 0);
        Register reg2 = new Register(2, 0);
        Register reg3 = new Register(3, 0);

        Register[] registers = {reg0, reg1, reg2, reg3};

        for (int i=0; i<instructions.size(); i++) {
            Instruction instruction = instructions.get(i);

            //lookup the operation code here
            String opname = hmKnownCodes.get(instruction.opcode);

            System.out.print("Running operation: " + opname + " " + Arrays.toString(instruction.args));

            AbstractOperation operation = AbstractOperation.getOperationForName(opname);

            Register rA = registers[instruction.args[0]];
            Register rB = registers[instruction.args[1]];
            Register rC = registers[instruction.args[2]];

            operation.setRegisterA(rA);
            operation.setRegisterB(rB);

            int result = operation.apply();
            System.out.print(" - Result: " + result);
            System.out.println();
            rC.content = result;
        }

        return reg0.getContent();
    }

    private void calculateCodes() {
        //Keep jigging codes around til we find the right ones
        HashSet<Integer> knownOpNums = new HashSet<>();
        while (hmKnownCodes.size() < 16) {

            for (String opname : opNameToPossibleOpNum.keySet()) {
                Set<Integer> possibleOps = opNameToPossibleOpNum.get(opname);
                if (possibleOps.size() == 1) {
                    Integer opnum = possibleOps.iterator().next();
                    hmKnownCodes.put(opnum, opname);
                    knownOpNums.add(opnum);
                }

                possibleOps.removeAll(knownOpNums);
                //System.out.println("Possible opnums for : " + opname + " : " + possibleOps.toString());
            }
        }
    }

    /**
     * Removes the impossible options from the possible options
     */
    private void resolve() {
        HashSet<Integer> triples = new HashSet<>();
        for (String opname : opNameToPossibleOpNum.keySet()) {
            Set<Integer> possibleOps = opNameToPossibleOpNum.get(opname);
            Set<Integer> impossibleOps = opNameToImpossibleOpNum.get(opname);

            possibleOps.removeAll(impossibleOps);
            impossibleOps.clear(); //useful for next part
            //System.out.println("Possible opnums for : " + opname + " : " + possibleOps.toString());
        }
    }

    @Override
    protected String getName() {
        return "Chronal Classification";
    }

    public static class Sample {
        public static final int SAMPLE_LINES = 3;
        private static final AdventParser BEFORE = AdventParser.ofRegex("Before: \\[(.*), (.*), (.*), (.*)\\]");
        private static final AdventParser AFTER  = AdventParser.ofRegex("After:  \\[(.*), (.*), (.*), (.*)\\]");

        private Register[] input = new Register[4];
        private Register[] output = new Register[4];
        private Instruction instruction;

        public static Sample of(String input, String output, String instruction) {
            int[] before = BEFORE.getNumericTokens(input);
            int[] after  = AFTER.getNumericTokens(output);

            Sample sample = new Sample();
            for (int r=0; r<4; r++) {
                sample.input[r] = new Register(r, before[r]);
                sample.output[r] = new Register(r, after[r]);
            }

            sample.instruction = Instruction.of(instruction);

            return sample;
        }

        public void processOperations() {
            Register regA = input[instruction.args[0]];
            Register regB = input[instruction.args[1]];
            Register regC = input[instruction.args[2]];

            int expectedResult = output[instruction.args[2]].getContent(); //instruction determines which register was written to
            //System.out.println("Result for op [" + instruction.opcode + "] is " + expectedResult + " ");
            int possibleOps = 0;
            for (AbstractOperation op : getOperations(regA, regB)) {
                int actualResult = op.apply();
                if (actualResult == expectedResult) {
                    addPossible(instruction.opcode, op.getOpName());
                } else {
                    addImpossible(instruction.opcode, op.getOpName());
                }
            }
            //System.out.println();
        }

        private void addPossible(int opnum, String opcode) {
            Set<Integer> possible = opNameToPossibleOpNum.get(opcode);
            if (possible == null) {
                possible = new HashSet<>();
                opNameToPossibleOpNum.put(opcode, possible);
            }
            possible.add(opnum);
        }

        private void addImpossible(int opnum, String opcode) {
            Set<Integer> possible = opNameToImpossibleOpNum.get(opcode);
            if (possible == null) {
                possible = new HashSet<>();
                opNameToImpossibleOpNum.put(opcode, possible);
            }
            possible.add(opnum);
        }

        private List<AbstractOperation> getOperations(Register rA, Register rB) {
            List<AbstractOperation> ops = new ArrayList<>();
            ops.add(new AddiOperation(rA, rB));
            ops.add(new AddrOperation(rA, rB));

            ops.add(new BaniOperation(rA, rB));
            ops.add(new BanrOperation(rA, rB));

            ops.add(new BoriOperation(rA, rB));
            ops.add(new BorrOperation(rA, rB));

            ops.add(new EqirOperation(rA, rB));
            ops.add(new EqriOperation(rA, rB));
            ops.add(new EqrrOperation(rA, rB));

            ops.add(new GtirOperation(rA, rB));
            ops.add(new GtriOperation(rA, rB));
            ops.add(new GtrrOperation(rA, rB));

            ops.add(new MuliOperation(rA, rB));
            ops.add(new MulrOperation(rA, rB));

            ops.add(new SetiOperation(rA, rB));
            ops.add(new SetrOperation(rA, rB));

            return ops;
        }
    }

    public static class Instruction {

        private static final AdventParser INSTRC = AdventParser.ofCustomRegex("(\\d+) (\\d+) (\\d+) (\\d+)", "(\\d+)");

        private int opcode;
        private int[] args;

        public Instruction(int opcode, int[] args) {
            this.opcode = opcode;
            this.args = args;
        }

        public static Instruction of(String instruction) {
            int[] instr  = INSTRC.getNumericTokens(instruction);
            return new Instruction(instr[0], Arrays.copyOfRange(instr, 1, instr.length));
        }
    }

    @Getter
    @Setter
    public static class Register {
        private int value;      //id
        private int content;    //register contents

        public Register(int value, int content) {
            this.value = value;
            this.content = content;
        }
    }
}

//354
//452
//493
