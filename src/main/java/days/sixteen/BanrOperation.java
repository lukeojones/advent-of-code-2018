package days.sixteen;

public class BanrOperation extends AbstractOperation {

    public BanrOperation(DaySixteen.Register rA, DaySixteen.Register rB) {
        super("banr", rA, rB);
    }

    @Override
    public int apply() {
        return rA.getContent() & rB.getContent();
    }
}
