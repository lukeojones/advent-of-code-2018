package days.sixteen;

public class GtrrOperation extends AbstractOperation {

    public GtrrOperation(DaySixteen.Register rA, DaySixteen.Register rB) {
        super("gtrr", rA, rB);
    }

    @Override
    public int apply() {
        return rA.getContent() > rB.getContent() ? 1 : 0;
    }
}
