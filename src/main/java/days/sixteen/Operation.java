package days.sixteen;

public class Operation {

    /**
     * Addition
     */

    /**
     * addr (add register) stores into register C the result of adding register A and register B.
     */
//    public int addr(DaySixteen.Register rA, DaySixteen.Register rB) {
//       return rA.getContent() + rB.getContent();
//    }
//
//    /**
//     * addi (add immediate) stores into register C the result of adding register A and value B.
//     */
//    public int addi(DaySixteen.Register rA, DaySixteen.Register rB) {
//        return rA.getContent() + rB.getValue();
//    }

    /**
     * Multiplication
     */

    /**
     * mulr (add register) stores into register C the result of adding register A and register B.
     */
//    public int mulr(DaySixteen.Register rA, DaySixteen.Register rB) {
//        return rA.getContent() * rB.getContent();
//    }
//
//    /**
//     * muli (add immediate) stores into register C the result of adding register A and value B.
//     */
//    public int muli(DaySixteen.Register rA, DaySixteen.Register rB) {
//        return rA.getContent() * rB.getValue();
//    }

    /**
     * Bitwise AND
     */

    /**
     * banr (bitwise AND register) stores into register C the result of the bitwise AND of register A and register B.
     */
//    public int banr(DaySixteen.Register rA, DaySixteen.Register rB) {
//        return rA.getContent() & rB.getContent();
//    }
//
//    /**
//     * bani (bitwise AND immediate) stores into register C the result of the bitwise AND of register A and value B.
//     */
//    public int bani(DaySixteen.Register rA, DaySixteen.Register rB) {
//        return rA.getContent() & rB.getValue();
//    }

    /**
     * Bitwise OR
     */

    /**
     * borr (bitwise OR register) stores into register C the result of the bitwise OR of register A and register B.
     */
//    public int borr(DaySixteen.Register rA, DaySixteen.Register rB) {
//        return rA.getContent() | rB.getContent();
//    }
//
//    /**
//     * bori (bitwise OR immediate) stores into register C the result of the bitwise OR of register A and value B.
//     */
//    public int bori(DaySixteen.Register rA, DaySixteen.Register rB) {
//        return rA.getContent() | rB.getValue();
//    }
//

    /**
     * Assignment
     */

    /**
     * setr (set register) copies the contents of register A into register C. (Input B is ignored.)
     */
//    public int setr(DaySixteen.Register rA, DaySixteen.Register rB) {
//        return rA.getContent();
//    }
//
//    /**
//     * seti (set immediate) stores value A into register C. (Input B is ignored.)
//     */
//    public int seti(DaySixteen.Register rA, DaySixteen.Register rB) {
//        return rA.getValue();
//    }


    /**
     * Greater-than testing:
     */

    /**
     * gtir (greater-than immediate/register) sets register C to 1 if value A is greater than register B. Otherwise, register C is set to 0.
     */
//    public int gtir(DaySixteen.Register rA, DaySixteen.Register rB) {
//        return rA.getValue() > rB.getContent() ? 1 : 0;
//    }
//
//    /**
//     * gtri (greater-than register/immediate) sets register C to 1 if register A is greater than value B. Otherwise, register C is set to 0.
//     */
//    public int gtri(DaySixteen.Register rA, DaySixteen.Register rB) {
//        return rA.getContent() > rB.getValue() ? 1 : 0;
//    }
//
//    /**
//     * gtrr (greater-than register/register) sets register C to 1 if register A is greater than register B. Otherwise, register C is set to 0.
//     */
//    public int gtrr(DaySixteen.Register rA, DaySixteen.Register rB) {
//        return rA.getContent() > rB.getContent() ? 1 : 0;
//    }

    /**
     * Equality testing
     */

    /**
     * eqir (equal immediate/register) sets register C to 1 if value A is equal to register B. Otherwise, register C is set to 0.
     */
//    public int eqir(DaySixteen.Register rA, DaySixteen.Register rB) {
//        return rA.getValue() == rB.getContent() ? 1 : 0;
//    }

    /**
     * eqri (equal register/immediate) sets register C to 1 if register A is equal to value B. Otherwise, register C is set to 0.
//     */
//    public int eqri(DaySixteen.Register rA, DaySixteen.Register rB) {
//        return rA.getContent() == rB.getValue() ? 1 : 0;
//    }
//
//    /**
//     * eqrr (equal register/register) sets register C to 1 if register A is equal to register B. Otherwise, register C is set to 0.
//     */
//    public int eqrr(DaySixteen.Register rA, DaySixteen.Register rB) {
//        return rA.getContent() == rB.getContent() ? 1 : 0;
//    }


}
