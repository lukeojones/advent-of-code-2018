package days.sixteen;

public class SetiOperation extends AbstractOperation {

    public SetiOperation(DaySixteen.Register rA, DaySixteen.Register rB) {
        super("seti", rA, rB);
    }

    @Override
    public int apply() {
        return rA.getValue();
    }
}
