package days.sixteen;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public abstract class AbstractOperation {

    private static HashMap<String, AbstractOperation> nameToOp = null;
    
    private String opname;
    protected DaySixteen.Register rA;
    protected DaySixteen.Register rB;

    public AbstractOperation(String opname, DaySixteen.Register rA, DaySixteen.Register rB) {
        this.opname = opname;
        this.rA = rA;
        this.rB = rB;
    }

    public void setRegisterA(DaySixteen.Register regA) {
        this.rA = regA;
    }

    public void setRegisterB(DaySixteen.Register regB) {
        this.rB = regB;
    }

    public String getOpName() {
        return opname;
    }

    public abstract int apply();

    public static AbstractOperation getOperationForName(String opname) {
        buildMap();
        return nameToOp.get(opname);
    }
    
    private static void buildMap() {
        if (nameToOp != null) {
            return;
        }

        nameToOp = new HashMap<>();
        for (AbstractOperation op: getOperations()) {
            nameToOp.put(op.opname, op);
        }
    }
    
    private static List<AbstractOperation> getOperations() {
        List<AbstractOperation> ops = new ArrayList<>();
        ops.add(new AddiOperation(null, null));
        ops.add(new AddrOperation(null, null));

        ops.add(new BaniOperation(null, null));
        ops.add(new BanrOperation(null, null));

        ops.add(new BoriOperation(null, null));
        ops.add(new BorrOperation(null, null));

        ops.add(new EqirOperation(null, null));
        ops.add(new EqriOperation(null, null));
        ops.add(new EqrrOperation(null, null));

        ops.add(new GtirOperation(null, null));
        ops.add(new GtriOperation(null, null));
        ops.add(new GtrrOperation(null, null));

        ops.add(new MuliOperation(null, null));
        ops.add(new MulrOperation(null, null));

        ops.add(new SetiOperation(null, null));
        ops.add(new SetrOperation(null, null));

        return ops;        
    }
}
