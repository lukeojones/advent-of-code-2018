package days.sixteen;

public class BaniOperation extends AbstractOperation {

    public BaniOperation(DaySixteen.Register rA, DaySixteen.Register rB) {
        super("bani", rA, rB);
    }

    @Override
    public int apply() {
        return rA.getContent() & rB.getValue();
    }
}
