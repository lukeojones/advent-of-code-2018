package days.sixteen;

public class EqirOperation extends AbstractOperation {

    public EqirOperation(DaySixteen.Register rA, DaySixteen.Register rB) {
        super("eqir", rA, rB);
    }

    @Override
    public int apply() {
        return rA.getValue() == rB.getContent() ? 1 : 0;
    }
}
