package days.sixteen;

public class BoriOperation extends AbstractOperation {

    public BoriOperation(DaySixteen.Register rA, DaySixteen.Register rB) {
        super("bori", rA, rB);
    }

    @Override
    public int apply() {
        return rA.getContent() | rB.getValue();
    }
}
