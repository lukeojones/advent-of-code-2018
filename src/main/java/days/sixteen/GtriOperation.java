package days.sixteen;

public class GtriOperation extends AbstractOperation {

    public GtriOperation(DaySixteen.Register rA, DaySixteen.Register rB) {
        super("gtri", rA, rB);
    }

    @Override
    public int apply() {
        return rA.getContent() > rB.getValue() ? 1 : 0;
    }
}
