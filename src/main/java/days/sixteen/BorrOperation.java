package days.sixteen;

public class BorrOperation extends AbstractOperation {

    public BorrOperation(DaySixteen.Register rA, DaySixteen.Register rB) {
        super("borr", rA, rB);
    }

    @Override
    public int apply() {
        return rA.getContent() | rB.getContent();
    }
}
