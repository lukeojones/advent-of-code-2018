package days.sixteen;

public class MulrOperation extends AbstractOperation {

    public MulrOperation(DaySixteen.Register rA, DaySixteen.Register rB) {
        super("mulr", rA, rB);
    }

    @Override
    public int apply() {
        return rA.getContent() * rB.getContent();
    }
}
