package days.sixteen;

public class MuliOperation extends AbstractOperation {

    public MuliOperation(DaySixteen.Register rA, DaySixteen.Register rB) {
        super("muli", rA, rB);
    }

    @Override
    public int apply() {
        return rA.getContent() * rB.getValue();
    }
}
