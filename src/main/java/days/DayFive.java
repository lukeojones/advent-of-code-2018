package days;

import java.util.List;

public class DayFive extends DayOfAdvent<Integer, Integer> {

    private static final char[] ALPHABET = "abcdefghijklmnopqrstuvwxyz".toCharArray();

    public DayFive(boolean testMode) {
        super("five", testMode);
    }

    @Override
    protected Integer solveFirst() {
        List<String> ids = getFileLinesList();
        if (ids.size() > 1 ) {
            System.out.println("More than one line!!");
            return -1;
        }
        return reactPolymer(ids.get(0));
    }

    @Override
    protected Integer solveSecond() {
        List<String> ids = getFileLinesList();
        if (ids.size() > 1 ) {
            System.out.println("More than one line!!");
            return -1;
        }
        return shortestPolymer(ids.get(0));
    }

    @Override
    protected String getName() {
        return "Alchemical Reduction";
    }

    private int shortestPolymer(String seq) {
        int minPolymerLength = Integer.MAX_VALUE;
        for (Character c : ALPHABET) {
            String cLower = "" + c;
            String cUpper = "" + Character.toUpperCase(c);
            String testSeq = seq.replaceAll(cLower, "");
            testSeq = testSeq.replaceAll(cUpper, "");
            int polymerLength = reactPolymer(testSeq);
            if (polymerLength < minPolymerLength) {
                minPolymerLength = polymerLength;
            }
        }

        return minPolymerLength;
    }

    private int reactPolymer(String seq) {
        int ix = 0;
        while (ix < seq.length() - 1) {
            Character first = seq.charAt(ix);
            Character second = seq.charAt(ix + 1);

            char firstLower = Character.toLowerCase(first);
            char secondLower = Character.toLowerCase(second);

            if (firstLower == secondLower
                    && first != second) {
                seq = seq.substring(0, ix) + seq.substring(ix + 2);
                ix = Math.max(0, ix-1);
            } else {
                ix++;
            }
        }
        return seq.length();
    }
}

//====================================================================================
//        Alchemical Reduction solution (a)
//        Answer:	10762
//        Time:		1720ms
//====================================================================================
//
//=====================================================================================
//        Alchemical Reduction solution (b)
//        Answer:	6946
//        Time:		8489ms
//=====================================================================================
