package days;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Tedious boiler-plate and file reading utilities
 */
public abstract class DayOfAdvent<S1,S2> {

    private String dayAsWords; //one, two, three etc
    private boolean testMode;

    public DayOfAdvent(String dayAsWords, boolean testMode) {
        this.dayAsWords = dayAsWords;
        this.testMode = testMode;
    }

    /**
     * Runs the first part solution and logs result and execution time
     */
    public final void solveAndLogFirst() {
        long start = System.currentTimeMillis();
        S1 solution1 = solveFirst();
        System.out.println();
        System.out.println("====================================================================================");
        System.out.println(getName() + " solution (a)");
        System.out.println("Answer:\t\t" + solution1);
        long fin = System.currentTimeMillis();
        System.out.println("Time:\t\t" + (fin-start) + "ms");
        System.out.println("====================================================================================");
        System.out.println();
    }

    /**
     * Runs the first part solution and logs result and execution time
     */
    public final void solveAndLogSecond() {
        long start = System.currentTimeMillis();
        S2 solution2 = solveSecond();
        System.out.println("=====================================================================================");
        System.out.println(getName() + " solution (b)");
        System.out.println("Answer:\t\t" + solution2);
        long fin = System.currentTimeMillis();
        System.out.println("Time:\t\t" + (fin-start) + "ms");
        System.out.println("=====================================================================================");
    }

    /**
     * Solution of type S1 for first part of problem
     * @return
     */
    protected abstract S1 solveFirst();

    /**
     * Solution of type S2 for second part of problem
     * @return
     */
    protected abstract S2 solveSecond();

    /**
     * Name of the puzzle
     * @return
     */
    protected abstract String getName();

    /**
     * Constructs filename using convention input_<dayAsWords>.txt
     * @return filename for real challenge input
     */
    private final String getChallengeFileName() {
        return "input_" + dayAsWords + ".txt";
    }

    /**
     * Constructs filename using convention input_<dayAsWords>.txt
     * @return filename for test challenge input
     */
    private final String getTestFileName() {
        return "input_" + dayAsWords + "_test.txt";
    }

    /**
     * Return relevant filename depending on whether we
     * are running in test mode or not.
     * @return filename for real or test challenge input
     */
    private final String getFileName() {
        return testMode ? getTestFileName() : getChallengeFileName();
    }

    private final String getResourcePath() {
        ClassLoader classLoader = getClass().getClassLoader();
        return classLoader.getResource(getFileName()).getFile();
    }

    /**
     * Return a stream of Strings for the lines in the input file
     * @return
     */
    protected final Stream<String> getFileLinesStream() {
        Stream<String> lines = Stream.empty();
        try {
            lines = Files.lines(Paths.get(getResourcePath()));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return lines;
    }

    /**
     * Return a list of Strings for the lines in the input file
     * @return
     */
    protected final List<String> getFileLinesList() {
        return getFileLinesStream()
                .collect(Collectors.toList());
    }

    /**
     * Return a Scanner of the given input file
     * @return
     */
    protected final Scanner getScannerForInput() {
        Scanner scanner = null;
        try {
            scanner = new Scanner(new File(getResourcePath()));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return scanner;
    }


}
