package days;

import utils.AdventParser;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class DayEleven extends DayOfAdvent<String,String> {

    private static int GRID_SIZE = 300;
    private static int SERIAL_NUM = 7857;

    private AdventParser parser = AdventParser.ofRegex("position=<(.*),(.*)> velocity=<(.*),(.*)>");
    private int[][] powerGrid = new int[300][300];

    public DayEleven(boolean test) {
        super("eleven", test);
    }

    @Override
    protected String solveFirst() {
        //Fill grid with power
        for (int x=1; x<=GRID_SIZE; x++) {
            for (int y=1; y<=GRID_SIZE; y++) {
                powerGrid[x-1][y-1] = getPower(x, y);;
            }
        }

        int[] maxValues = findMaxValues(powerGrid, IntStream.rangeClosed(3, 3));
        return maxValues[0] + "," + maxValues[1];
    }

//    @Override
//    protected String solveSecond() {
//        int[] maxValues = findMaxValues(powerGrid, IntStream.rangeClosed(1, GRID_SIZE));
//        return maxValues[0] + "," + maxValues[1] + "," + maxValues[3];
//    }

    @Override
    protected String solveSecond() {
        int[][] sat = new int[301][301];

        int maxX = Integer.MIN_VALUE;
        int maxY = Integer.MIN_VALUE;
        int maxWindowSize = Integer.MIN_VALUE;
        int maxPower = Integer.MIN_VALUE;

        //Compute the Summed Area Table
        for(int x = 1; x <= 300; x++) {
            for(int y = 1; y <= 300; y++) {
                sat[x][y] = getPower(x, y) + sat[x - 1][y] + sat[x][y - 1] - sat[x - 1][y - 1];
            }
        }

        //Compute the intensities
        for(int windowSize = 1; windowSize <= 300; windowSize++) {
            for(int x = windowSize; x <= 300; x++) {
                for(int y = windowSize; y <= 300; y++) {
                    //Calculate the intensity at a point SAT(A) + SAT(D) - SAT(B) - SAT(C)
                    int total = sat[x][y] + sat[x - windowSize][y - windowSize] - sat[x - windowSize][y] - sat[x][y - windowSize] ;
                    if(total > maxPower) {
                        maxPower = total;
                        maxX = x;
                        maxY = y;
                        maxWindowSize = windowSize;
                    }
                }
            }
        }

        return (maxX - maxWindowSize + 1) + "," + (maxY - maxWindowSize + 1) + "," + maxWindowSize;
    }

    @Override
    protected String getName() {
        return "Chronal Charge";
    }

    /**
     * Find the values providing maximum power
     * @param powerGrid
     * @param windowSizeRange
     * @return array of xMax, yMax, powerMax, windowMax
     */
    private int[] findMaxValues(int[][] powerGrid, IntStream windowSizeRange) {
        int powerMax = Integer.MIN_VALUE;
        int xMax = 0;
        int yMax = 0;
        int windowMax = 0;

        List<Integer> windowSizes = windowSizeRange.boxed().collect(Collectors.toList());
        for (int i=0; i<windowSizes.size(); i++) {
            int windowSize = windowSizes.get(i);
            for (int x=1; x<=GRID_SIZE - windowSize; x++) {
                for (int y = 1; y <= GRID_SIZE - windowSize; y++) {
                    int power = getTotalPower(powerGrid, x - 1, y - 1, windowSize);
                    if (power > powerMax) {
                        powerMax = power;
                        windowMax = windowSize;
                        xMax = x;
                        yMax = y;
                    }
                }
            }
        }

        return new int[]{xMax, yMax, powerMax, windowMax};
    }

    private int getTotalPower(int[][] powerGrid, int arrX, int arrY, int windowSize) {
        int power = 0;
        for (int x=0;x<windowSize;x++) {
            for (int y=0;y<windowSize;y++) {
                power += powerGrid[arrX+x][arrY+y];
            }
        }
        return power;
    }

    private int getPower(int x, int y) {
        int rackId = x + 10;
        int power = rackId;
        power = power * y;
        power = power + SERIAL_NUM;
        power = power * rackId;
        power = (power / 100) % 10;
        return power - 5;
    }
}


//====================================================================================
//        Chronal Charge solution (a)
//        Answer:	243,16
//        Time:		81ms
//====================================================================================
//
//=====================================================================================
//        Chronal Charge solution (b)
//        Answer:	231,227,14
//        Time:		26264ms
//=====================================================================================
