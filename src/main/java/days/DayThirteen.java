package days;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class DayThirteen extends DayOfAdvent<String, String> {

    private Map<Integer, Cart> cartMap = new HashMap<>();
    private char[][] map;
    private int[][] cartLayer;

    public DayThirteen(boolean testMode) {
        super("thirteen", testMode);
    }

    protected static char UP = '^';
    protected static char DOWN = 'v';
    protected static char LEFT = '<';
    protected static char RIGHT = '>';

    protected static char HZ = '-';
    protected static char VT = '|';
    protected static char BS = '\\';
    protected static char FS = '/';
    protected static char XX = '+';

    protected static String LEFT_TURN = "LEFT";
    protected static String RIGHT_TURN = "RIGHT";
    protected static String STRAIGHT_TURN = "STRAIGHT";

    public static boolean isDirection(char c) {
        return c == UP || c == DOWN || c == LEFT || c == RIGHT;
    }

    private static int[] UP_DIR      = new int[]{0,-1};
    private static int[] DOWN_DIR      = new int[]{0,1};
    private static int[] LEFT_DIR      = new int[]{-1,0};
    private static int[] RIGHT_DIR      = new int[]{1,0};

    public class Cart {

        private char symbol;
        private int id;
        private int x;
        private int y;
        private String prevXXAction;

        protected boolean isLeft() {
            return symbol == LEFT;
        }

        protected boolean isRight() {
            return symbol == RIGHT;
        }

        protected boolean isUp() {
            return symbol == UP;
        }

        protected boolean isDown() {
            return symbol == DOWN;
        }

        public Cart(int[] direction, int id, int x, int y, char symbol) {
            this.symbol = symbol;
            this.id = id;
            this.x = x;
            this.y = y;
        }

        protected void move() {
            int[] direction = new int[]{0,0};
            if (isLeft()) {
                direction = LEFT_DIR;
            } else if (isRight()) {
                direction = RIGHT_DIR;
            } else if (isUp()) {
                direction = UP_DIR;
            } else if (isDown()) {
                direction = DOWN_DIR;
            } else {
                System.out.println("No direction to move in");
            }

            x = x + direction[0];
            y = y + direction[1];

            changeDirection();
        }

        private String getTurnAction() {
            String action = null;
            if (prevXXAction == null) {
                action = LEFT_TURN;
            } else if (prevXXAction.equals(LEFT_TURN)) {
                action = STRAIGHT_TURN;
            } else if (prevXXAction.equals(STRAIGHT_TURN)) {
                action = RIGHT_TURN;
            } else if (prevXXAction.equals(RIGHT_TURN)) {
                action = LEFT_TURN;
            } else {
                System.out.println("Found bad prev direction");
            }

            prevXXAction = action;
            return action;
        }

        private char getSymbolAfterTurn() {
            String turnAction = getTurnAction();
            if (isUp()) {
                if (turnAction.equals(LEFT_TURN)) {
                    return LEFT;
                } else if (turnAction.equals(RIGHT_TURN)) {
                    return RIGHT;
                }
            } else if (isDown()) {
                if (turnAction.equals(LEFT_TURN)) {
                    return RIGHT;
                } else if (turnAction.equals(RIGHT_TURN)) {
                    return LEFT;
                }
            } else if (isLeft()) {
                if (turnAction.equals(LEFT_TURN)) {
                    return DOWN;
                } else if (turnAction.equals(RIGHT_TURN)) {
                    return UP;
                }
            } else if (isRight()) {
                if (turnAction.equals(LEFT_TURN)) {
                    return UP;
                } else if (turnAction.equals(RIGHT_TURN)) {
                    return DOWN;
                }
            }

            //System.out.println("Going straight on");
            return symbol;
        }

        protected void changeDirection() {
            char mapSymbol = map[y][x];
            if (mapSymbol == XX) {
                symbol = getSymbolAfterTurn();
            }
            else if (isRight()) {
                ;
                if (mapSymbol == BS) {
                    symbol = DOWN;
                } else if (mapSymbol == FS) {
                    symbol = UP;
                }
            }else if (isLeft()) {
                if (mapSymbol == BS) {
                    symbol = UP;
                } else if (mapSymbol == FS) {
                    symbol = DOWN;
                }
            } else if (isUp()) {
                if (mapSymbol == BS) {
                    symbol = LEFT;
                } else if (mapSymbol == FS) {
                    symbol = RIGHT;
                }
            } else if (isDown()) {
                if (mapSymbol == BS) {
                    symbol = RIGHT;
                } else if (mapSymbol == FS) {
                    symbol = LEFT;
                }
            }
        }

    }

    @Override
    protected String solveFirst() {
        parseOriginalMap();
        printMap(true);

        //Will be in loop soon
        int iteration = 0;
        while (moveCarts() == null) {
            System.out.println("Iteration: " + iteration++);
        }

        return "ANSWER";
    }

    private String moveCarts() {
        int[][] newCartLayer = new int[map.length][map[0].length];
        try {
            for (int y=0; y<map.length; y++) {
                for (int x=0; x<map[0].length; x++) {
                    int cartId = cartLayer[y][x];
                    if (cartId > 0) {
                        Cart cart = cartMap.get(cartId);
                        cart.move(); //update coords on the cart

                        //check for collisions here
                        if (newCartLayer[cart.y][cart.x] > 0) {
                            System.out.println("Collision at: " + cart.x + "," + cart.y);
                            return x + "," + y;
                        }
                        newCartLayer[cart.y][cart.x] = cartId;
                    }
                }
            }
            cartLayer = newCartLayer;
        }
        catch (ArrayIndexOutOfBoundsException ex) {
            ex.printStackTrace();
        }

        return null;
    }

    private String moveCartsHelpful() {
        System.out.println("Carts in map: " + cartMap.size());
        int[][] newCartLayer = new int[map.length][map[0].length];
        try {
            for (int y=0; y<map.length; y++) {
                for (int x=0; x<map[0].length; x++) {
                    int cartId = cartLayer[y][x];
                    if (cartId > 0) {
                        Cart cart = cartMap.get(cartId);
                        System.out.println("processing cart: " + cartId + "(" + cart.symbol + ") @ " + x + "," + y);
                        cart.move();

                        //check for collisions here
                        int unmovedCartCollision = cartLayer[cart.y][cart.x];
                        int newlyMovedCartCollision = newCartLayer[cart.y][cart.x];
                        if (unmovedCartCollision > 0) {
                            System.out.println("Collision with unmoved cart: " + unmovedCartCollision + " at: " + cart.x + "," + cart.y);
                            newCartLayer[cart.y][cart.x] = 0;
                            cartLayer[cart.y][cart.x] = 0;
                            cartMap.remove(cartId);
                            cartMap.remove(unmovedCartCollision);
                            System.out.println("Now have carts in map: " + cartMap.size());
                        } else if (newlyMovedCartCollision > 0) {
                            System.out.println("Collision with moved cart: " + newlyMovedCartCollision + " at: " + cart.x + "," + cart.y);
                            newCartLayer[cart.y][cart.x] = 0;
                            cartMap.remove(cartId);
                            cartMap.remove(newlyMovedCartCollision);
                            System.out.println("Now have carts in map: " + cartMap.size());
                        }
                        else {
                            newCartLayer[cart.y][cart.x] = cartId;
                        }
                    }
                }
            }

            if (cartMap.size() == 1) {
                cartMap.forEach((k,v) -> System.out.println("Last cart " + v.id + " @: " + v.x + "," + v.y));
                return "DONE";
            }

            cartLayer = newCartLayer;
        }
        catch (ArrayIndexOutOfBoundsException ex) {
            ex.printStackTrace();
        }

        return null;
    }

    private void parseOriginalMap() {
        List<String> lines = getFileLinesList();

        int height = lines.size();
        int width = Integer.MIN_VALUE;

        for (int i=0; i<height; i++) {
            String line = lines.get(i);
            int length = line.length();
            if (length > width) {
                width = length;
            }
        }

        //Build a 2d array representing the map (without carts)
        //And also store up all the carts
        int cartId = 1;
        map = new char[height][width];
        cartLayer = new int[height][width];
        for (int y=0; y<height; y++) {
            char[] chars = lines.get(y).toCharArray();
            for (int x = 0; x < width && x < chars.length; x++) {
                char c = chars[x];
                Cart cart = createCart(c, cartId, x, y);
                if (cart != null) {
                    cartMap.put(cartId, cart);
                    cartLayer[y][x] = cartId;
                    cartId++;
                    c = transformCartCharIntoMapChar(c);
                }
                map[y][x] = c;
            }
        }
    }

    private static char transformCartCharIntoMapChar(char c) {
        if (c == UP || c == DOWN) {
            return VT;
        } else if (c == LEFT || c == RIGHT) {
            return HZ;
        }

        System.out.println("Cannot transform " + c);
        return 'E';
    }

    /**
     * Create a Cart from either ^, v, < or >
     * @param c
     * @param x
     * @param y
     */
    private Cart createCart(char c, int cartId, int x, int y) {
        if (!isDirection(c)) {
            return null;
        }

        return new Cart(getDirection(c), cartId, x, y, c);
    }

    private int[] getDirection(char c) {
        if (c == UP) {
            return UP_DIR;
        } else if (c == DOWN) {
            return DOWN_DIR;
        } else if (c == LEFT) {
            return LEFT_DIR;
        } else if (c == RIGHT) {
            return RIGHT_DIR;
        }

        System.out.println("Found direction I don't recognise!");
        return new int[]{0,0};
    }

    @Override
    protected String solveSecond() {
        parseOriginalMap();
        //printMap(true);

        //Will be in loop soon
        int iteration = 0;
        while (moveCartsHelpful() == null) {
            System.out.println("Iteration: " + iteration++);
        }

        printMap(true);

        return "ANSWER";
    }

    @Override
    protected String getName() {
        return "Mine Cart Madness";
    }

    /**
     * Printing utilities
     * @param includeCarts
     */
    private void printMap(boolean includeCarts) {

        System.out.println("Cart Map is....");
        if (cartMap.size() == 1) {
            cartMap.forEach((k,v) -> System.out.println("Cart " + v.id + " @: " + v.x + "," + v.y));
        }

        StringBuilder sb = new StringBuilder();

        for(int i = 0; i<map.length; i++)
        {
            for(int j = 0; j<map[0].length; j++)
            {
                char c = map[i][j];
                if (includeCarts) {
//                    int cartId = cartLayer[i][j];
//                    if (cartId > 0) {
//
//                        c = cartMap.get(cartId).symbol;
//                    }
                    Iterator<Integer> carts = cartMap.keySet().iterator();
                    while (carts.hasNext()) {
                        Cart cart = cartMap.get(carts.next());
                        if (cart.x == j && cart.y == i) {
                            sb.append("Added cart: " + cart.id + " @ " + j + "," + i);
                            c = cart.symbol;
                        }
                    }
                }

                System.out.print(c);
            }
            System.out.println();
        }

        System.out.println(sb.toString());
    }

    private void printCarts() {
        for(int i = 0; i<cartLayer.length; i++)
        {
            for(int j = 0; j<cartLayer[0].length; j++)
            {
                System.out.print(cartLayer[i][j]);
            }
            System.out.println();
        }
    }
}
