package days.nineteen;

import days.DayOfAdvent;
import days.sixteen.AbstractOperation;
import days.sixteen.DaySixteen;
import utils.AdventParser;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DayNineteen extends DayOfAdvent<Integer, Integer> {

    private AdventParser parser = AdventParser.ofRegex("(.*) (.*) (.*) (.*)");
    private Map<String,Integer> instructionCount = new HashMap<>();

    public DayNineteen(boolean testMode) {
        super("nineteen", testMode);
    }

    @Override
    protected Integer solveFirst() {
        return runProgram();
    }

    private Integer runProgram() {
        DaySixteen.Register[] registers = createRegisters(17);

        //For part 2
        registers[0].setContent(1);

        List<String> instructions = getFileLinesList();

        //Sort instruction pointer reg
        int ipReg = AdventParser.ofRegex("#ip (.*)").getNumericTokens(instructions.remove(0))[0];
        DaySixteen.Register regIP = registers[ipReg];
        System.out.println("Setting IP Register to: " + ipReg);

        long iterations = 0;
        int pointer = 0;
        while (pointer < instructions.size()) {

            //Store pointer into ip reg
            regIP.setContent(pointer);

            //Execute the instruction
            String input = instructions.get(pointer);
            String[] tokens = parser.getTokens(input, true);
            String opcode = tokens[0];
            int rAIx = Integer.valueOf(tokens[1]);
            int rBIx = Integer.valueOf(tokens[2]);
            int rCIx = Integer.valueOf(tokens[3]);

            AbstractOperation operation = AbstractOperation.getOperationForName(opcode);
            //System.out.print("Running operation: " + opcode + " " + rAIx + " : " + rBIx + " : " + rCIx);

            DaySixteen.Register rA = registers[rAIx];
            DaySixteen.Register rB = registers[rBIx];
            DaySixteen.Register rC = registers[rCIx];

            operation.setRegisterA(rA);
            operation.setRegisterB(rB);

            int result = operation.apply();
            //System.out.print(" - Result: " + result);
            //System.out.println();
            rC.setContent(result);

            //Write ipReg content back to pointer
            pointer = regIP.getContent();

            Integer count = instructionCount.getOrDefault("" + pointer + "_" + input, 0);
            instructionCount.put("" + pointer + "_" + input, count+1);

            //Increment pointer
            pointer++;
            //System.out.println("Pointer is now: " + pointer);

//            if (iterations++ % 1000 == 0) {
//                System.out.println("Register 0 is " + registers[0].getContent() + " after " + iterations + " iterations");
//            }

            //Print Registers
//            printRegisters(registers);
        }

//        for (int i=0; i<instructions.size(); i++) {
//
//            String[] tokens = parser.getTokens(instructions.get(pointer), true);
//            String opcode = tokens[0];
//            int rAIx = Integer.valueOf(tokens[1]);
//            int rBIx = Integer.valueOf(tokens[2]);
//            int rCIx = Integer.valueOf(tokens[3]);
//
//            AbstractOperation operation = AbstractOperation.getOperationForName(opcode);
//            System.out.print("Running operation: " + opcode + " " + rAIx + " : " + rBIx + " : " + rCIx);
//
//            DaySixteen.Register rA = registers[rAIx];
//            DaySixteen.Register rB = registers[rBIx];
//            DaySixteen.Register rC = registers[rCIx];
//
//            operation.setRegisterA(rA);
//            operation.setRegisterB(rB);
//
//            int result = operation.apply();
//            System.out.print(" - Result: " + result);
//            System.out.println();
//            rC.setContent(result);
//        }

        return registers[0].getContent();
    }

    private void printRegisters(DaySixteen.Register[] registers) {
        System.out.println();
        for (int i=0; i<6; i++) {
            System.out.print(registers[i].getContent() + " ");
        }
    }

    /**
     * Bit of a hack because value can now be bigger than the register number
     * @param nRegisters
     * @return
     */
    private DaySixteen.Register[] createRegisters(int nRegisters) {
        DaySixteen.Register[] registers = new DaySixteen.Register[nRegisters];
        for (int i=0; i<nRegisters; i++) {
            DaySixteen.Register reg = new DaySixteen.Register(i, 0);
            registers[i] = reg;
        }
        return registers;
    }

    @Override
    protected Integer solveSecond() {
        return null;
    }

    @Override
    protected String getName() {
        return "Go With The Flow";
    }
}
