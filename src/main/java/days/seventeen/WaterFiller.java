package days.seventeen;

import java.awt.*;
import java.util.Optional;

public class WaterFiller {

    private Cell[][] grid;
    private Cell spring;
    private int height;
    private int width;
    //boolean[][] visited =

    public WaterFiller(Cell[][] grid, Cell spring) {
        this.grid = grid;
        this.height = grid.length;
        this.width = grid[0].length;
        this.spring = spring;
    }

    //base case when y > maxy
    public void fill(Cell loc) {

        Optional<Cell> south = loc.getSouth();
        if (!south.isPresent()) {
            loc.mark();
            return;
        }

        //If the south square is sand, set it to visited and move
        if (south.map(Cell::available).get()) {
            if (!loc.isSpring()) {
                loc.mark();
            }
            fill(south.get());
        }

        //if one to south is edge or water, split left and right
        if (south.map(Cell::isBase).get()) {
            loc.mark();

            Optional<Cell> east = loc.getEast();
            if (east.map(Cell::isSand).get()) {
                fill(east.get());
            }

            Optional<Cell> west = loc.getWest();
            if (west.map(Cell::isSand).get()) {
                fill(west.get());
            }

            if (closed(loc)) {
                flood(loc);
            }
        }

//        if (closed(loc)) {
//            flood(loc);
//        }

        //check we have a end at each left and right

        //printGrid(grid);
    }

    private void flood(Cell loc) {
        leftFlood(loc);
        rightFlood(loc);
    }

    private void rightFlood(Cell loc) {
        Point arrPt = loc.getRealPoint();
        int offset = 0;
        while (true) {
            Cell c = grid[arrPt.y][arrPt.x-(offset++)];
            if (c.isClay()) {
                return;
            }
            c.flood();
        }
    }

    private void leftFlood(Cell loc) {
        Point arrPt = loc.getRealPoint();
        int offset = 0;
        while (true) {
            Cell c = grid[arrPt.y][arrPt.x-(offset--)];
            if (c.isClay()) {
                return;
            }
            c.flood();
        }
    }

    private boolean closed(Cell loc) {
        return leftClosed(loc) && rightClosed(loc);
    }

    private boolean leftClosed(Cell loc) {
        Point arrPt = loc.getRealPoint();
        int offset = 0;
        while (arrPt.x + offset > -1) {
            Cell c = grid[arrPt.y][arrPt.x + offset];
            if (c.isSand()) {
                return false;
            }

            if (c.isClay()) {
                return true;
            }
            offset--;
        }

        return false;
    }

    private boolean rightClosed(Cell loc) {
        Point arrPt = loc.getRealPoint();
        int offset = 0;
        while (arrPt.x + offset < grid[0].length) {
            Cell c = grid[arrPt.y][arrPt.x + offset];
            if (c.isSand()) {
                return false;
            }

            if (c.isClay()) {
                return true;
            }
            offset++;
        }

        return false;
    }

    private void printGrid(Cell[][] grid) {
        for(int i = 0; i<grid.length; i++)
        {
            for(int j = 0; j<grid[0].length; j++)
            {
                System.out.print(grid[i][j]);
            }
            System.out.println();
        }
    }
}
