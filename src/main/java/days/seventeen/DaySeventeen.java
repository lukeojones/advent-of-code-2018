package days.seventeen;

import days.DayOfAdvent;
import javafx.util.Pair;
import utils.AdventParser;

import java.awt.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DaySeventeen extends DayOfAdvent<Long, Long> {
    AdventParser parser_vz = AdventParser.ofCustomRegex("x=(\\d+), y=(\\d+)..(\\d+)", "(\\d+)");
    AdventParser parser_hz = AdventParser.ofCustomRegex("y=(\\d+), x=(\\d+)..(\\d+)", "(\\d+)");
    private static final int WIDTH_BUFFER = 1;
    private static final Point SPRING_PT = new Point(500,0);


    protected static int XMIN = Integer.MAX_VALUE;
    protected static int XMAX = Integer.MIN_VALUE;
    protected static int YMIN = Integer.MAX_VALUE;
    protected static int YMAX = Integer.MIN_VALUE;

    public DaySeventeen(boolean testMode) {
        super("seventeen", testMode);
    }

    @Override
    protected Long solveFirst() {
        Pair<Cell[][], Cell> gridAndSpring = parseGrid();
        Cell[][] grid = gridAndSpring.getKey();
        Cell spring = gridAndSpring.getValue();

        //printGrid(grid);

        WaterFiller wf = new WaterFiller(grid, spring);
        wf.fill(spring);

        //printGrid(grid);

        return countWater(grid);
    }

    private long countWater(Cell[][] grid) {
        long count = 0;
        for (int y = YMIN; y < grid.length; y++) {
            for (int x = 0; x < grid[0].length; x++) {
                Cell cell = grid[y][x];
                if (cell.isWater() || cell.isStream()) {
                    count++;
                }
            }
        }
        return count;
    }


    private Pair<Cell[][], Cell> parseGrid() {
        List<String> lines = getFileLinesList();
        Map<Point, Cell> mapClay = new HashMap<>();

        for (int l=0; l<lines.size(); l++) {
            String line = lines.get(l);
            if (parser_vz.matches(line)) {
                int[] toks = parser_vz.getNumericTokens(line);
                int x = toks[0];
                int y1 = toks[1];
                int y2 = toks[2];
                for (int y=y1;y<=y2;y++) {
                    Point pt = new Point(x, y);
                    mapClay.put(pt, new Cell('#', pt));
                }

                XMIN = Math.min(XMIN, x);
                YMIN = Math.min(YMIN, y1);
                XMAX = Math.max(XMAX, x);
                YMAX = Math.max(YMAX, y2);
            } else if (parser_hz.matches(line)){
                int[] toks = parser_hz.getNumericTokens(line);
                int y = toks[0];
                int x1 = toks[1];
                int x2 = toks[2];
                for (int x=x1;x<=x2;x++) {
                    Point pt = new Point(x, y);
                    mapClay.put(pt, new Cell('#', pt));
                }
                XMIN = Math.min(XMIN, x1);
                YMAX = Math.max(YMAX, y);
                YMIN = Math.min(YMIN, y);
                XMAX = Math.max(XMAX, x2);
            } else {
                throw new IllegalStateException();
            }
        }

        System.out.println("XMAX: " + XMAX + " XMIN: " + XMIN);
        System.out.println("YMAX: " + YMAX + " YMIN: " + YMIN);
        int height = YMAX +1;
        int width = WIDTH_BUFFER + (XMAX - XMIN +1) + WIDTH_BUFFER;

        Cell[][] grid = new Cell[height][width];
        Cell spring = null;
        //Add sand
        for (int y=0;y<grid.length; y++) {
            for (int x=0;x<width; x++) {
                Point pt = new Point(x+ XMIN -1, y);
                Cell c = new Cell('.', pt);
                if (SPRING_PT.equals(pt)) {
                    c = new Cell('+', pt);
                    spring = c;
                } else if (mapClay.get(pt) != null) {
                    c = new Cell('#', pt);
                }

                c.setGrid(grid);
                grid[y][x] = c;
            }
        }

        return new Pair<>(grid, spring);
    }

    private void printGrid(Cell[][] grid) {
        for(int i = 0; i<grid.length; i++)
        {
            //System.out.print(i);
            //System.out.print("   ");
            int j = 0;
            for(j = 0; j<grid[0].length; j++)
            {
                System.out.print(grid[i][j]);
            }
            //System.out.print("   ");
            //System.out.print(j);
            System.out.println();
        }
    }

    @Override
    protected Long solveSecond() {
        Pair<Cell[][], Cell> gridAndSpring = parseGrid();
        Cell[][] grid = gridAndSpring.getKey();
        Cell spring = gridAndSpring.getValue();

        //printGrid(grid);

        WaterFiller wf = new WaterFiller(grid, spring);
        wf.fill(spring);
        return countSettledWater(grid);
    }

    private long countSettledWater(Cell[][] grid) {
        long count = 0;
        for (int y = YMIN; y < grid.length; y++) {
            for (int x = 0; x < grid[0].length; x++) {
                Cell cell = grid[y][x];
                if (cell.isWater()) {
                    count++;
                }
            }
        }
        return count;
    }

    @Override
    protected String getName() {
        return "Reservoir Research";
    }
}
//====================================================================================
//Reservoir Research solution (a)
//Answer:	39367
//Time:		215ms
//====================================================================================
//
//=====================================================================================
//Reservoir Research solution (b)
//Answer:	33061
//Time:		91ms
//=====================================================================================