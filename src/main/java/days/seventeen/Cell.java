package days.seventeen;

import lombok.Getter;

import java.awt.*;
import java.util.Optional;

@Getter
public class Cell {

    private static final char WATER = '~';
    private static final char CLAY = '#';
    private static final char SAND = '.';
    private static final char STREAM = '|';
    private static final char SPRING = '+';

    private char type;
    private Point pt;
    private Cell[][] grid;

    public Cell(char type, Point pt) {
        this.type = type;
        this.pt = pt;
    }

    /**
     * Useful to have a reference back to the grid here
     * @param grid
     */
    public void setGrid(Cell[][] grid) {
        this.grid = grid;
    }

    public Point getPt() {
        return pt;
    }

    @Override
    public String toString() {
        return "" + type;
    }

    public Optional<Cell> moveSouth() {
        return move(getSouth());
    }

    public Optional<Cell> getSouth() {
        Point real = getRealPoint();
        if (!boundCheck(real.y+1, real.x)) {
            return Optional.empty();
        }
        return Optional.of(grid[real.y+1][real.x]);
    }

    private boolean boundCheck(int y, int x) {
        boolean maxBound = (y < grid.length) && (x < grid[0].length);
        boolean minBound = (y > -1) && (x > -1);
        return maxBound && minBound;
    }

    public boolean available() {
        return isSand() || isStream();
    }

    public boolean isSand() {
        return type == SAND;
    }

    public boolean isWater() {
        return type == WATER;
    }

    public boolean isSpring() {
        return type == SPRING;
    }

    /**
     * Have we hit water or clay
     * @return
     */
    public boolean isBase() {
        return isWater() || isClay();
    }

    public boolean isClay() {
        return type == CLAY;
    }

    public boolean isStream() {
        return type == STREAM;
    }

    public Point getRealPoint() {
        return new Point(pt.x - DaySeventeen.XMIN + 1, pt.y);
    }

    public Optional<Cell> moveEast() {
        return move(getEast());
    }

    public Optional<Cell> moveWest() {
        return move(getWest());
    }

    public Optional<Cell> getEast() {
        Point real = getRealPoint();
        if (!boundCheck(real.y, real.x+1)) {
            return Optional.empty();
        }
        return Optional.of(grid[real.y][real.x+1]);
    }

    public Optional<Cell> getWest() {
        Point real = getRealPoint();
        if (!boundCheck(real.y, real.x-1)) {
            return Optional.empty();
        }
        return Optional.of(grid[real.y][real.x-1]);
    }

    private Optional<Cell> move(Optional<Cell> dest) {
        if (dest.isPresent()) {
            Cell cell = dest.get();
            if (cell.available()) {
                cell.type = STREAM;
                return Optional.of(cell);
            }
        }
        return Optional.empty();
    }

    public void mark() {
        this.type = STREAM;
    }

    public void flood() {
        this.type = WATER;
    }
}
